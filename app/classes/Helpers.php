<?php
class Helpers {
	static function mainmenutype($type){
		switch ($type) {
			case '1':
				return Lang::get('msg.msg_content', array(), 'th');
				break;
			
			case '2':
				return Lang::get('msg.msg_url', array(), 'th');
				break;
			case '3':
				return Lang::get('msg.msg_submenu', array(), 'th');
				break;
			 
		}
	}
	public static function create_url($string){
	$con = array(' ','%','/','&','*','#','+');
	$rp = array('-','-','-','-','-','-','-');
  	$slug=str_replace($con, $rp, $string);
   return $slug;
}
	public static function filestype($type) {
		$path = URL::to('img');
		if($type =="MP4" || $type =="mp4" ||$type =="WMV" ||$type =="wmv" 
			||$type =="AVI" ||$type =="avi" ||$type =="MKV" ||$type =="mkv" ||$type =="MOV" ||$type =="mov"  )
			 {
			 	return "<img src=\"$path/vdo.png\" border=\"0\"> ";
		}else if ($type=="PDF" || $type =="pdf"){
				return "<img src=\"$path/pdf.png\" border=\"0\"> ";
		}else if ($type=="DOC" || $type =="doc" || $type =="docx"|| $type =="DOCX"){
				return "<img src=\"$path/docx.png\" border=\"0\"> ";
		}else if ($type=="XLS" || $type =="xls" || $type =="xlsx"|| $type =="XLSX"){
				return "<img src=\"$path/xls.png\" border=\"0\"> ";
		}else if ($type=="PPT" || $type =="ppt" || $type =="pptx"|| $type =="PPTX"){
				return "<img src=\"$path/pptx.png\" border=\"0\"> ";
		}else if($type=="0"){
				return "<img src=\"$path/none.png\" border=\"0\"> ";
		}
	}

	public static function filestype_l($type) {
		$path = URL::to('img');
		if($type =="MP4" || $type =="mp4" ||$type =="WMV" ||$type =="wmv" 
			||$type =="AVI" ||$type =="avi" ||$type =="MKV" ||$type =="mkv" ||$type =="MOV" ||$type =="mov"  )
			 {
			 	return "<img src=\"$path/mkv-icon.png\" border=\"0\" style=\"height:155px\" class=\"img-responsive\"> ";
		}else if ($type=="PDF" || $type =="pdf"){
				return "<img src=\"$path/pdf-icon.png\" border=\"0\" style=\"height:155px\"  class=\"img-responsive\"> ";
		}else if ($type=="DOC" || $type =="doc" || $type =="docx"|| $type =="DOCX"){
				return "<img src=\"$path/docx-icon.png\" border=\"0\" style=\"height:155px\"  class=\"img-responsive\"> ";
		}else if ($type=="XLS" || $type =="xls" || $type =="xlsx"|| $type =="XLSX"){
				return "<img src=\"$path/xls-icon.png\" border=\"0\" style=\"height:155px\"  class=\"img-responsive\" > ";
		}else if ($type=="PPT" || $type =="ppt" || $type =="pptx"|| $type =="PPTX"){
				return "<img src=\"$path/pptx-icon.png\" border=\"0\" style=\"height:155px\"  class=\"img-responsive\"> ";
		}else if($type=="0"){
				return "<img src=\"$path/none-icon.png\" border=\"0\" style=\"height:155px\"  class=\"img-responsive\"> ";
		}
	}

	public static function create_tag()
	{
		$tagMax = Tag::max('tag_count');
		
		$tag = Tag::all();
		foreach($tag as $tags => $t){
			if($tagMax>0) {
			$percent = floor(($t['tag_count'] / $tagMax) * 100);
			}else{
				$percent = 0;
			}
			$url = URL::to('tag',array($t->id,$t->tag_url));
		//echo  $t->tag_name;
			if ($percent < 20): 
			   $class = 'smallest'; 
			 elseif ($percent >= 20 and $percent < 40):
			   $class = 'small'; 
			 elseif ($percent >= 40 and $percent < 60):
			   $class = 'medium';
			 elseif ($percent >= 60 and $percent < 80):
			   $class = 'large';
			 else:
			 $class = 'largest';
			 endif;

			 echo "<span class=\"$class\"><a href=\"$url\"><i class=\"fa fa-tag\"></i>".$t->tag_name."</a></span>";

		}

	}
	public static function DateFormat($date)
{
	$time=strtotime($date);
	return date("d M Y",$time);
}
}