@extends('masterbackend')
@section('content')
	     <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
             {{ $title}}
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol>
        </section>

        <!-- Main content -->
       <section class="content">
			 
          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">{{ Lang::get('msg.msg_add',array(), 'th') }}</h3>
               
               
              

             
            </div>
            
            <div class="row" >
              <div class="col-md-5 col-sm-6 col-xs-12" 
              @if(Session::has('status'))
              id ="null"
              @endif
              @if(Session::has('save-success'))
               id="status_save" 
              @endif
              @if(Session::has('edit-success'))
               id="status_save" 
              @endif
              @if(Session::has('del-success'))
               id="status_save" 
              @endif
                style="margin-top:10px;margin-left:30%;  display:none" >
                 @if(Session::has('save-success'))
                  <div class="info-box bg-green">
                 @endif
                 @if(Session::has('edit-success'))
                  <div class="info-box bg-teal">
                 @endif
                 @if(Session::has('del-success'))
                  <div class="info-box bg-red-active">
                 @endif
                <span class="info-box-icon">
                  @if(Session::has('save-success'))
                  <i class="fa fa-save"></i>
                  @endif
                  @if(Session::has('del-success'))
                  <i class="fa fa-trash"></i>
                  @endif
                  @if(Session::has('edit-success'))
                  <i class="fa fa-pencil">
                  @endif
                  </i>
                </span>
                <div class="info-box-content">
                  <span class="info-box-text">{{ Lang::get('msg.msg_result', array(), 'th') }}</span>
                  <span class="info-box-number">
                    @if(Session::has('save-success'))
                    {{ Lang::get('msg.msg_save_success', array(), 'th') }}
                    @endif
                    @if(Session::has('del-success'))
                    {{ Lang::get('msg.msg_del_success', array(), 'th') }}
                    @endif
                    @if(Session::has('edit-success'))
                    {{ Lang::get('msg.msg_edit_success', array(), 'th') }}
                    @endif
                  </span>
                   </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
              @if(Session::has('save-success') || Session::has('del-success') || Session::has('edit-success'))
               </div>
              @endif

              
            </div>
            <div class="box-body">
               <div class="box box-primary">
                 
              
                  {{ Form::open(array(
                    'id'=>'form-mainmenu'
                    ,'role'=>'form'
                    ,'url'=>'backend/menu/add/mainmenu'
                    ), $rules)}}
                  <div class="box-body">
                    <div class="form-group">
                      <label for="">{{ Lang::get('msg.mainmenu_name', array(), 'th') }}</label>
                      {{Form::input('text', 'txt_name', '', 
                         array(
                               'class'=>'form-control'

                              ))}}
                    </div>
                    <div class="form-group required">
                      <label for="">{{ Lang::get('msg.msg_type', array(), 'th') }}</label>
                      <select name="txt_type" id="bt-url" class="form-control">
                        <option value=""></option>
                        <option value="1">{{ Lang::get('msg.msg_content', array(), 'th') }}</option>
                        <option value="2">{{ Lang::get('msg.msg_url', array(), 'th') }}</option>
                        <option value="3">{{ Lang::get('msg.msg_submenu', array(), 'th') }}</option>
                      </select>
                       
                       </div>
                    
                 
                  <div class="box-body" id="content" style="display:none">
               
                    <textarea id="editor1" name="txt_detail" rows="10" cols="80"></textarea>
                  
                </div>
                <div class="box-body" id="url" style="display:none">
               
                   <input type="text" name="txt_url" class="form-control" value="http://">
                  
                </div>
             
                    
                    
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                     <button type="button" id="bt-reset" class="btn btn-danger  pull-right">
                                                <i class="fa fa-close"></i>
                                                {{ Lang::get('msg.msg_cancle',array(), 'th')}}
                                            </button>
                                            <button type="submit" class="btn btn-primary">
                                              <i class="fa fa-check-circle"></i>
                                                {{ Lang::get('msg.msg_submit',array(), 'th')}}
                                            </button>
                                            {{ Form::close()}}
                  </div>
                </form>
              </div><!-- /.box -->
              
            </div><!-- /.box-body -->
             
          </div><!-- /.box -->

        </section><!-- /.content -->
    </div>
{{ Session::get('status') }}
   <input type="hidden" id="lang" value="{{ Lang::get('msg.msg_input_mainmenu',array(),'th') }}">
   <input type="hidden" id="status" name="status" value="{{ Session::get('status') }}">

@stop
@section('script')
<script src="https://cdn.ckeditor.com/4.4.3/standard/ckeditor.js"></script>
         <script type="text/javascript">
         CKEDITOR.replace('editor1');
         var msg = $('#lang').val();
         var status =$('#status').val();
     	   var validate =   $('#form-mainmenu').validate({

            highlight: function(element) {
              jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');

            },
            success: function(element) {
              jQuery(element).closest('.form-group').removeClass('has-error');
            },
            events   : 'submit',
            selector : 'input[type!=submit]',
            messages: {
            txt_name: "<span class=\"glyphicon glyphicon-question-sign\"></span> "+msg,
      			txt_type: "<span class=\"glyphicon glyphicon-question-sign\"></span> "+msg
   			},
            callback : function( elem, valid ) {
                if ( ! valid ) {
                    $( elem ).addClass('error');
                }
            }
          });



    
            
              $('#status_save').show(0).delay(2000).slideUp();
               
              $('#bt-url').change(function(){
                type = $(this).val();
                if(type==2) {
                $('#content').hide();
                $('#url').show();
              }else if(type==1){
                $('#url').hide();
                $('#content').show();
              }else if(type==3){
                $('#url').hide();
                $('#content').hide();
              }
              });
              $('#bt-reset').click(function(){
                $(this).closest('#form-mainmenu').find("input[type=text], textarea").val("");
                window.location.href='{{ URL::to("backend/menu/mainmenu")}}';
              });
              
               
             
     
        </script>
@stop