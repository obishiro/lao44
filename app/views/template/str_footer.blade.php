    <script src="{{ asset('plugins/jQuery/jQuery-2.1.4.min.js') }} "></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="{{ asset('js/bootstrap.min.js') }} "></script>
    <!-- FastClick -->
   
    <!-- AdminLTE App -->
    <script src="{{ asset('js/app.min.js') }} "></script>
    <!-- Sparkline -->
 
    
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
  
    <!-- AdminLTE for demo purposes -->
    <script src="{{ asset('js/demo.js') }} "></script>
   <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }} "></script>
    <script src="{{ asset('plugins/datatables/dataTables.bootstrap.min.js') }} "></script>
      <script src="{{ asset('js/jquery.validate.min.js') }}"></script>
      <script src="{{ asset('js/jquery.validate.laravel.js') }}"></script>
     <script src="{{ asset('plugins/ckeditor/ckeditor.js') }}"></script>
       
      @yield('script')
  </body>
</html>
