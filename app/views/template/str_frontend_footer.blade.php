<script src="{{ asset('frontend/js/jquery-1.11.1.min.js') }} "></script>
<script src="{{ asset('frontend/js/jquery-migrate-1.2.1.min.js') }} "></script>	
<script src="{{ asset('frontend/js/bootstrap.min.js') }} "></script>
<script src="{{ asset('frontend/js/bootstrap-hover-dropdown.min.js') }} "></script>
<script src="{{ asset('frontend/js/jquery.magnific-popup.min.js') }} "></script>
<script src="{{ asset('frontend/js/owl.carousel.min.js') }} "></script>
<script src="{{ asset('frontend/js/custom.js') }} "></script>
 <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }} "></script>
<script src="{{ asset('plugins/datatables/dataTables.bootstrap.min.js') }} "></script>

@yield('script')
</body>
</html>