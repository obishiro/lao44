<div class="col-md-3">
			<!-- Categories Links Starts -->
				<h3 class="side-heading">{{ Lang::get('frontend.categories',array(),'th') }}</h3>
				<div class="list-group categories">
					@foreach($categories as $cat =>$c )

					<a href="{{ URL::to('categories',array($c->id,$c->categories_url)) }}" class="list-group-item">
						<i class="fa fa-chevron-right"></i>
						{{ $c->categories_name}}
					</a>
					@endforeach
					
				</div>
			<!-- Categories Links Ends -->
			<!-- Special Products Starts -->
				<h3 class="side-heading">{{ Lang::get('frontend.tags',array(),'th')}}</h3>
				<ul class="side-products-list">
				<!-- Special Product #1 Starts -->
					<li class="clearfix" id="tagcloud">
						 {{ Helpers::create_tag()}}
					</li>
				 
				</ul>
			 
			 
				
			<!-- Shopping Options Ends -->
			</div>