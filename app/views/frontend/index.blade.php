@extends('masterfrontend',['categories'=>$Categories,'mainmenu'=>$Mainmenu])
@section('title',$env->web_name_lo)
@section('description',$env->web_detail)
@section('keyword',$env->web_keyword)

@section('content')
<div class="col-md-9">
@include('frontend/content_vdo',array('DataVdo'=>$DataVdo))
@include('frontend/content_product_list',array('DataContent'=>$Categories_index,'number'=>$number))
@include('frontend/content_recome',array('DataRecome'=>$DataRecome))
</div>
 
@stop