<section class="products-list">			
				<!-- Heading Starts -->
					<h2 class="product-head">Specials Products</h2>
				<!-- Heading Ends -->
				<!-- Products Row Starts -->
					<div class="row">
					@foreach($DataContent as $data => $dc)
						<div class="col-md-4 col-sm-6">
							<div class="product-col">
								<div class="image">
									 <center>
									{{ Helpers::filestype_l($dc->files_type)}}
								</center>
								</div>
								<div class="caption">
									<h4>
										<a href="{{ URL::to('content',array($dc->id,$dc->content_url))}}">{{ $dc->content_name}}</a>
									</h4>
									 
									 	<div class="cart-button">
												<button type="button" class="btn btn-cart">
													{{ number_format($dc->content_view)}}
													<i class="fa fa-eye"></i> 
												</button>									
											</div>
								</div>
							</div>
						</div>
					 	@endforeach
					 
					</div>
				<!-- Products Row Ends -->
				</section>