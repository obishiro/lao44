 
<section class="product-carousel">
				<!-- Heading Starts -->
					<h2 class="product-head">{{ Lang::get('frontend.last_vdo',array(),'th') }}</h2>
				<!-- Heading Ends -->
				<!-- Products Row Starts -->
					<div class="row">
						<div class="col-xs-12">
						<!-- Product Carousel Starts -->
							 
								@foreach($DataVdo as $data => $vd)
								<div class="col-md-4 col-sm-12">
									<div class="product-col">
										 
										<div class="caption" >
											<h4><a href="{{ URL::to('vdo',array($vd->id,$vd->content_url))}}">{{ $vd->content_name}}</a></h4>
											<div class="description">
											<video  src="{{ URL::to('uploadfiles',array($vd->files_newname))}}" width="230" height="150" controls="controls"></video>
											</div>
											 
											<div class="cart-button">
												<button type="button" class="btn btn-cart">
													{{ number_format($vd->content_view)}}
													<i class="fa fa-eye"></i> 
												</button>									
											</div>
										</div>
									</div>
								</div>
								@endforeach

							
						 
						<!-- Product Carousel Ends -->
							<div class="row">
								<div class="col-xs-12"  style="padding-right:17px;padding-bottom:5px;">
								<a href="" class="btn btn-success" style="position:relative;float:right"><i class="fa fa-list"></i> {{ Lang::get('frontend.all_vdo',array(),'th') }}</a>
								</div>
							</div>
						</div>

					</div>
				<!-- Products Row Ends -->
				</section>
 