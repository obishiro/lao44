<body>
<!-- Header Section Starts -->
	<header id="header-area" style="margin-top:-32px">
	 
	<!-- Header Top Ends -->
	<!-- Main Header Starts -->
		<div class="main-header">
			<div class="container">
				<div class="row">
				<!-- Search Starts -->
					
				<!-- Search Ends -->
				<!-- Logo Starts -->
					<div class="col-md-1">
						<div id="logo">
						 <a href="/"><img src="{{ URL::to('img/logo2.jpg') }}" style="height:50px"  title="" alt="" class="img-responsive" /></a>  
						</div>
					</div>
				<div class="col-md-6">
						<div id="search" style="margin-top:18px;margin-left:-30px">
							{{ Form::open(array(
								'method' =>'POST',
								'url'	=>'search'
								))}}
							<div class="input-group">
							  <input type="text" name="keyword" class="form-control input-lg" placeholder="Search">
							  <span class="input-group-btn">
								<button class="btn btn-lg" type="submit">
									<i class="fa fa-search"></i>
								</button>
							  </span>
							</div>
							{{ Form::close() }}
						</div>	
					</div>
				</div>
			</div>
		</div>
	<!-- Main Header Ends -->
	<!-- Main Menu Starts -->
		<nav id="main-menu" class="navbar" role="navigation"  style="margin-top:-25px">
			<div class="container">
			<!-- Nav Header Starts -->
				<div class="navbar-header">
					<button type="button" class="btn btn-navbar navbar-toggle" data-toggle="collapse" data-target=".navbar-cat-collapse">
						<span class="sr-only">Toggle Navigation</span>
						<i class="fa fa-bars"></i>
					</button>
				</div>
			<!-- Nav Header Ends -->
			<!-- Navbar Cat collapse Starts -->
				<div class="collapse navbar-collapse navbar-cat-collapse">
					<ul class="nav navbar-nav">
						<li><a href="{{ URL::to('/') }}">{{ Lang::get('frontend.home', array(),'th') }}</a></li>
						@foreach($mainmenu as $menu =>$m)
							@if($m->mainmenu_type==1)	
						<li><a href="{{ URL::to('mainmenu',array($m->id,$m->m_url)) }}">
							{{ $m->mainmenu_name }}
						     </a>
						</li>
							@endif
							@if($m->mainmenu_type==2)	
						<li><a href="{{ $m->mainmenu_url }}" target="_blank">
							{{ $m->mainmenu_name }}
						     </a>
						</li>
							@endif
						
							@if($m->mainmenu_type==3)	
						  <li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="10">
								{{ $m->mainmenu_name}}
							</a>
							<ul class="dropdown-menu" role="menu">
								<?php 
									$Submenu = Submenu::where('submenu_categories',$m->id)->get();
								?>
								@foreach($Submenu as $_smenu => $sm)
									@if($sm->submenu_type=="1")
								<li><a tabindex="-1" href="{{ URL::to('submenu',array($sm->id,$sm->s_url))}}">{{ $sm->submenu_name}}</a></li>
									@endif
									@if($sm->submenu_type=="2")
								<li><a tabindex="-1" href="{{ $sm->submenu_url }}" target="_blank">{{ $sm->submenu_name}}</a></li>
									@endif
								@endforeach
								 
							</ul>
						</li> 
							@endif
						@endforeach
						 
					</ul>
				</div>
			<!-- Navbar Cat collapse Ends -->
			</div>
		</nav>
	<!-- Main Menu Ends -->
	</header>
	<div id="main-container-home" class="container">
		<div class="row">
			