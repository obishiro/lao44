<section class="products-list">			
				<!-- Heading Starts -->
					<h2 class="product-head">Lastest Document</h2>
				<!-- Heading Ends -->
				<!-- Products Row Starts -->
					<div class="row">
					@foreach($DataContent as $data => $dc)
						<div class="col-md-6 col-sm-6">
							<table width="100%" class="table">
								<thead>
									<th colspan="3" style="background:#ECE8E5"><img src="{{ URL::to('img/title-icon.png')}}" alt=""> {{$dc->categories_name }} {{ $dc->id}}</th>

								</thead>
								<tbody>
									<?php $DataContent_file = Uploadfiles::select('tb_files.files_newname','tb_files.files_oldname','tb_content.id'
									,'tb_content.content_name','tb_content.content_view','tb_files.files_type','tb_content.content_url','tb_content.created_at')
									
							->join('tb_content','tb_content.content_file','=','tb_files.token')
							->orderBy('tb_content.id','desc')
							->where('tb_content.content_categories',$dc->id)
							->take(10)->skip(0)
							->get();
							?>
							@foreach($DataContent_file as $datafile =>$df)
									<tr>
										<td width="32">{{ Helpers::filestype($df->files_type) }}</td>
										<td><a href="{{ URL::to('content',array($df->id,$df->content_url))}}">{{ $df->content_name }}</a>
										</td>
										<td width="100">
											{{ Helpers::DateFormat($df->created_at) }}
										</td>
										 
									</tr>
							@endforeach
								</tbody>
							</table>
							 
						</div>
					 	@endforeach
					 
					</div>
				<!-- Products Row Ends -->
				</section>
