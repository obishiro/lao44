 
<section class="product-carousel">
				<!-- Heading Starts -->
					<h2 class="product-head">{{ Lang::get('frontend.recome_doc',array(),'th') }}</h2>
				<!-- Heading Ends -->
				<!-- Products Row Starts -->
					<div class="row">
						<div class="col-xs-12">
						<!-- Product Carousel Starts -->
							 
								@foreach($DataRecome as $data => $vd)
								<div class="col-md-4 col-sm-12">
									<div class="product-col">
										 
										<div class="caption" >
											<h4><a href="{{ URL::to('vdo',array($vd->id,$vd->content_url))}}">{{ $vd->content_name}}</a></h4>
											<div class="description">
											<center>
											<?php if($vd->files_type=='mp4'||
											$vd->files_type=='MP4'||
											$vd->files_type=='wmv'||
											$vd->files_type=='WMV'||
											$vd->files_type=='avi'||
											$vd->files_type=='AVI'||
											$vd->files_type=='mkv'||
											$vd->files_type=='MKV'||
											$vd->files_type=='mov'||
											$vd->files_type=='MOV'){?>

														 
											<video  src="{{ URL::to('uploadfiles',array($vd->files_newname))}}" 
												width="230" height="150" controls="controls">
											</video>
											
											<?php
											}else{
											?>
											{{ Helpers::filestype_l($vd->files_type) }}
											<?php } ?>
											</center>
											
											</div>
											 
											<div class="cart-button">
												<button type="button" class="btn btn-cart">
													{{ number_format($vd->content_view)}}
													<i class="fa fa-eye"></i> 
												</button>									
											</div>
										</div>
									</div>
								</div>
								@endforeach

							
						 
						<!-- Product Carousel Ends -->
						</div>
					</div>
				<!-- Products Row Ends -->
				</section>
 