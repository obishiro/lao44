@extends('masterfrontend',['categories'=>$Categories,'mainmenu'=>$Mainmenu])
@section('title',$c->categories_name)
@section('description',$env->web_detail)
@section('keyword',$env->web_keyword)

@section('content')
<div class="col-md-9">
 <!-- Breadcrumb Starts -->
				<ol class="breadcrumb">
					<li><a href="{{ URL::to('/') }}">{{ Lang::get('frontend.home', array(),'th') }}</a></li>
				 	<li>{{ Lang::get('frontend.categories',array(),'th') }}</li>
				 	<li class="active">{{ $c->categories_name }}</li> 
				</ol>
			<!-- Breadcrumb Ends -->
		 
			<!-- Product Filter Ends -->
			<!-- Product List Display Starts -->
				<div class="row">
				<!-- Product #1 Starts -->

					
					<div class="col-xs-12">
						<h4>{{ $c->categories_name }}</h4>
				 
							{{-- <div class="image">
								<a href="{{ URL::to('content',array($dc->id,$dc->content_url))}}">
								 {{ Helpers::filestype($dc->files_type)}}
								</a>
							</div>

							<div class="caption">
								<h4><a href="{{ URL::to('content',array($dc->id,$dc->content_url))}}">{{ $dc->content_name}}</a></h4>
							 
									

							 
								 
								 
							</div> --}}
							<table width="100%" class="table table-bordered table-striped" id="data_categories">
								<thead>
									<th>#</th>
									<th>File</th>
									<th>Content name</th>
									<th>Submitted By</th>
									<th>Submitted On</th>
									<th>File Size</th>
									<th>Downloads</th>
									 
								</thead>
								</table>
						 
					</div>
					
				</div>
			 
			</div>
		<!-- Primary Content Ends -->
		</div>
</div>

@stop
@section('script')
         <script type="text/javascript">
         var my_table= $('#data_categories').dataTable({
            "bProcessing": true,
            "bServerSide": true,
            "iDisplayLength": 50,
            "targets": 0,
            "sAjaxSource": "{{ $api }}",
            columns: [
            {data:'no',name:'no'},
            {data:'files_type',name:'files_type'},
            {data: 'content_name', name: 'content_name'},
            {data: 'created_by', name: 'created_by'},
            
            {data: 'created_at', name: 'created_at'},
            
            {data: 'files_size', name: 'files_size'},
            {data: 'content_view', name: 'content_view'}
        ],
        "fnDrawCallback":function(){
         table_rows = my_table.fnGetNodes(); 
          $.each(table_rows, function(index){
          $("td:first", this).html(index+1);
          });
         }

            });
        </script>
 @stop