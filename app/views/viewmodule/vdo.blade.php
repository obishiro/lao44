@extends('masterfrontend',['categories'=>$Categories,'mainmenu'=>$Mainmenu])
@section('title',$data->content_name)
@section('description',$env->web_detail)
@section('keyword',$env->web_keyword)

@section('content')
<div class="col-md-9">
			<!-- Breadcrumb Starts -->
				<ol class="breadcrumb">
					<li><a href="/">{{ Lang::get('frontend.home',array(),'th') }}</a></li>
					<li><a href="{{URL::to('categories',array($data->cid,$data->categories_url))}}">{{ $data->categories_name }}</a></li>
					<li class="active">{{ $data->content_name}}</li>
				</ol>
			<!-- Breadcrumb Ends -->
			<!-- Product Info Starts -->
				<div class="row product-info">
				<!-- Left Starts -->
					<div class="col-sm-2 images-block">
						<center>
						<p>
							{{ Helpers::filestype_l($data->files_type)}}
						</p>
						<p><button type="button" class="btn btn-info" id="bt_download_file"><i class="fa fa-download"></i> Download</button>
						</p>
						</center>
					</div>

				<!-- Left Ends -->
				<!-- Right Starts -->
					<div class="col-sm-10 product-details">
					<!-- Product Name Starts -->
						<h2>{{ $data->content_name}}</h2>
					<!-- Product Name Ends -->
						<hr />
					<!-- Manufacturer Starts -->
						<ul class="list-unstyled manufacturer">
							<li>
								<span>Author:</span> Administrator
							</li>
							<li><span>Year:</span> 2016</li>
							<li><span>Submitted On:</span> {{ $data->created_at}}</li>
							<li>
								<span>File Size:</span> {{number_format($data->files_size/1024)}} Kb.
							</li>
							<li>
								<span>Downloads:</span> {{ number_format($data->content_view)}}
							</li>
						</ul>
					<!-- Manufacturer Ends -->
						<hr />
					 
					<!-- Available Options Starts -->
						<div class="options">
						 <label class="control-label text-uppercase" for="input-quantity">{{ Lang::get('frontend.tags',array(),'th')}}:</label>
						</div>
						<div class="cart-button button-group">
										@foreach($tags as $tag =>$t)
										<a type="button" class="btn btn-cart" href="{{URL::to('tag',array($t->tagid,$t->tag_url))}}">
											{{$t->tag_name}}
											<i class="fa fa-tag"></i> 
										</a>
										@endforeach									
									</div>
					 
					</div>
				<!-- Right Ends -->
				</div>
			<!-- product Info Ends -->
			<!-- Product Description Starts -->
				<div class="product-info-box">
					<h4 class="heading">{{ Lang::get('msg.content_detail',array(),'th')}}</h4>
					<div class="content panel-smart">
						<center>
						 
						<video  src="{{ URL::to('uploadfiles',array($data->files_newname))}}" width="100%"  height="350" controls="controls"></video>
					</center>
						{{ $data->content_detail}}
					</div>
				</div>

			 
				
			<!-- Related Products Ends -->
			</div>
@stop
@section('script')
 <script type="text/javascript">
$(document).ready(function() {
 	var file = "{{ URL::to('uploadfiles',array($data->files_newname))}}";
	$('#bt_download_file').click(function(){
   	window.location.href=file;
    });
});
 </script>
@stop