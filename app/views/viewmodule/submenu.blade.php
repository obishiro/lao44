@extends('masterfrontend',['categories'=>$Categories,'mainmenu'=>$Mainmenu])
@section('title',$data->submenu_name)
@section('description',$env->web_detail)
@section('keyword',$env->web_keyword)

@section('content')
<div class="col-md-9">
			<!-- Breadcrumb Starts -->
				<ol class="breadcrumb">
					<li><a href="/">{{ Lang::get('frontend.home',array(),'th') }}</a></li>
					 
					<li class="active">{{ $data->submenu_name}}</li>
				</ol>
			<!-- Breadcrumb Ends -->
			<!-- Product Info Starts -->
			
			<!-- product Info Ends -->
			<!-- Product Description Starts -->
				<div class="product-info-box">
					<h4 class="heading">{{ Lang::get('msg.content_detail',array(),'th')}}
						<small>Created by: Administrator | Created date: {{ $data->created_at}}</small>

					</h4>
					<div class="content panel-smart">
						 
						{{ $data->submenu_detail}}
					</div>
				</div>

			 
				
			<!-- Related Products Ends -->
			</div>
@stop
 