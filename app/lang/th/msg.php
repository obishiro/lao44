<?php

return array(
	'txt-please-input' 		=>'Please input',
	'username'			=> 'Username',
	'password'			=> 'Password',
	'signin'				=> 'Sign In',
	

	/////////// process display  //////////
	'error_login_incorrect' 		=> 'Your username or password incorrect',
	'msg_add'			=> 'Add new item',
	'msg_edit'			=> 'Edit',
	'msg_del'			=> 'Delete',
	'msg_error'			=> 'Error',
	'msg_cancle'		=> 'Cancel',
	'msg_submit'		=> 'Save changes',
	'msg_save_success'		=> 'Save data successfully',
	'msg_edit_success'		=> 'Edit data successfully',
	'msg_del_success'		=> 'Delete data successfully',
	'msg_no'				=> 'No.',
	'msg_show'			=> 'Show',
	'msg_unshow'			=> 'Unshow',
	'msg_created'			=> 'Created at',
	'msg_updated'			=> 'Updated at',
	'msg_tools'				=> 'Tools',
	'msg_confirm'			=> 'Are you sure?',
	'msg_result'			=> 'Result',
	'msg_logout'			=> 'Log out',
	'msg_type'				=> 'Type',
	'msg_content'			=> 'Content',
	'msg_url'				=> 'Url/Link',
	'msg_submenu'			=> 'Sub menu',
	'msg_sort'				=> 'Sort',
	/////////// Main Menu ////////
	'dashboard'		=> 'Configuration',
	'config-env'		=> 'Config Enviroment',
	'config-categories'	=> 'Config Categories',
	'config-tag'		=> 'Config Tag',
	'config-password'		=> 'Change password',
	'msg_old_password'		=> 'Old password',
	'msg_new_password'		=> 'New password',
	'list-item'		=> 'List item',
	'mainmenu'		=> 'Main menu',
	'submenu'		=> 'Sub menu',
	'categories_name' => 'Categories name',
	'tag_name' => 'Tag name',
	'mainmenu_name' => 'Mainmenu name',
	'msg_input_categories' => 'Categories name is required',
	'msg_input_tag' => 'Tag name is required',
	'msg_input_mainmenu' => 'This field is required',

	/////////// Content  ///////////
	'content'		=> 'Content',
	'content_name'		=> 'Content name',
	'content_author'		=> 'Author name',
	'content_year'		=> 'Year',
	'content_categories'	=> 'Categories',
	'content_detail'		=> 'Detail',
	'content_file'		=> 'File',
	'content_type'		=> 'Type',
	'content_view'		=> 'View',

	/////////////////////// Enviroment ///////////////////
	'web_name_lo'		=> 'Web name (lao)',
	'web_name_en'		=> 'Web name (Eng)',
	'web_address'		=> 'Address',
	'web_tel'			=> 'Telephone/Mobile',
	'web_email'			=> 'Email',
	'web_keyword'		=> 'Web keyword',
	'web_detail'		=> 'Web detail',



 

	);