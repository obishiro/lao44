<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');
Route::group(array('prefix'=>'backend','before' => 'auth'), function()
{
     
    Route::get('backend','BackendController@index');
    Route::get('logout','BackendController@getLogout');

    // Route::get('config/{type}',function($type){
    	 
    // 	return Redirect::to('config/categories');
    // })->where(array('type' =>'[A-Za-z]+'));
    Route::get('config/{type}', 'BackendController@getConfig')->where(array('type' =>'[A-Za-z]+'));
    Route::post('config/{type}', 'BackendController@postConfig')->where(array('type' =>'[A-Za-z]+'));
    Route::post('menu/{type}', 'BackendController@postConfig')->where(array('type' =>'[A-Za-z]+'));

    Route::get('menu/{type}', 'MenuContoller@getMenu')->where(array('type' =>'[A-Za-z]+'));
    Route::get('menu/add/{type}', 'MenuContoller@getAdd')->where(array('type' =>'[A-Za-z]+'));
    Route::get('menu/edit/{type}/{id}', 'MenuContoller@getEdit')->where(array('type' =>'[A-Za-z]+','id'=>'[0-9]+'));
    Route::post('menu/add/{type}', 'MenuContoller@postAdd')->where(array('type' =>'[A-Za-z]+'));
    Route::post('menu/edit/{type}', 'MenuContoller@postEdit')->where(array('type' =>'[A-Za-z]+'));
    
    Route::get('data/categories','ApiController@getDatacategories');
    Route::get('data/tag','ApiController@getDatatag');
    Route::get('data/mainmenu','ApiController@getDatamainmenu');
    Route::get('data/submenu','ApiController@getDatasubmenu');
    Route::get('data/content','ApiController@getDatacontent');

    Route::get('del/{type}/{id}','BackendController@getDel')->where(array('type' =>'[A-Za-z]+','id'=>'[0-9]+'));
    Route::get('show/{type}/{id}','BackendController@getShow')->where(array('type' =>'[A-Za-z]+','id'=>'[0-9]+'));
    Route::get('unshow/{type}/{id}','BackendController@getUnshow')->where(array('type' =>'[A-Za-z]+','id'=>'[0-9]+'));
    Route::post('edit/{type}/','BackendController@postEdit')->where(array('type' =>'[A-Za-z]+'));

    Route::get('content','ContentController@index');
    Route::get('content/addcontent','ContentController@getAdd');
    Route::get('content/editcontent/{id}','ContentController@getEdit')->where(array('id'=>'[0-9]+'));
    Route::post('content/addcontent','ContentController@postAdd');
    Route::post('content/editcontent','ContentController@postEdit');
    Route::post('content/addfile/{key}','ContentController@postAddfile');
    Route::post('content/editfile/{key}','ContentController@postEditfile');
    Route::post('content/dropimages/{key}','ContentController@postDropimages');
    Route::post('content/ckupload','ContentController@postCkupload');



});
Route::get('login',function(){
	return View::make('backend.login');
});
Route::post('login','BackendController@postLogin');

Route::get('mainmenu/{id}/{url}', 'ViewController@getViewmainmenu')->where(array('id'=>'[0-9]+'));
Route::get('submenu/{id}/{url}', 'ViewController@getViewsubmenu')->where(array('id'=>'[0-9]+'));
Route::get('content/{id}/{url}', 'ViewController@getViewcontent')->where(array('id'=>'[0-9]+'));
Route::get('vdo/{id}/{url}', 'ViewController@getViewvdo')->where(array('id'=>'[0-9]+'));
Route::get('categories/{id}/{url}', 'ViewController@getViewcategories')->where(array('id'=>'[0-9]+'));
Route::get('tag/{id}/{url}', 'ViewController@getViewtag')->where(array('id'=>'[0-9]+'));
Route::get('data/viewcategories/{id}/{url}','ApiController@getDataviewcategories')->where(array('id'=>'[0-9]+'));
Route::get('data/viewtag/{id}/{url}','ApiController@getDataviewtag')->where(array('id'=>'[0-9]+'));
Route::get('data/viewsearch/{text}','ApiController@getDataviewsearch');

Route::post('search','ViewController@postViewsearch');
