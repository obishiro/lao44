<?php

class ContentController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /content
	 *
	 * @return Response
	 */
	public function index()
	{
		$title = Lang::get('msg.content',array(),'th');
		$api = URL::to('backend/data/content');
		return View::make('backend.content.maincontent')->with(
			array(
			'title' 	=>$title,
			'api'	=> $api,
			'status'	=> 'null'
			));


	}
	public function getAdd()
	{
		 		$title = Lang::get('msg.content',array(),'th');
				$sql = Categories::orderBy('id','desc')->get();
				$tag = Tag::orderBy('id','desc')->get();
				$rules = ['txt_name'=>'required','txt_type'=>'required'];
				 
				return View::make('backend.content.addcontent')->with(
				 array(
				 	'title' 	=>$title,
				 	'rules'		=>$rules,
				  	'sql'		=> $sql,
				  	'tag'		=> $tag,
				 	'status'	=> 'null'
				       ));
	}
	public function getEdit($id)
	{
		 		$title = Lang::get('msg.content',array(),'th');
				$sql = Categories::orderBy('id','desc')->get();
				$tag = Tag::orderBy('id','desc')->get();
				$tagcontent = Tagcontent::where('content_id',$id)->get();
				$c = Content::find($id);
				$rules = ['txt_name'=>'required','txt_type'=>'required'];
				 
				return View::make('backend.content.editcontent')->with(
				 array(
				 	'title' 	=>$title,
				 	'rules'		=>$rules,
				  	'sql'		=> $sql,
				  	'tag'		=> $tag,
				 	'status'	=> 'null',
				 	'tagcontent' => $tagcontent,
				 	'c'			=> $c
				       ));
	}
	public function postAdd()
	{
		$url = Helpers::create_url(Input::get('txt_name'));
		 $tag = Input::get('txt_tag');
	 	$token = Input::get('key');

	 	
		 $c = new Content;
		 $c->content_name 			= Input::get('txt_name');
		 $c->content_categories		= Input::get('txt_type');
		 $c->content_detail			= Input::get('txt_detail');
		 $c->content_file			= $token;
		 $c->content_view			= '0';
		 $c->content_show			= '0';
		 $c->content_author			= Input::get('txt_author');
		 $c->content_year			= Input::get('txt_year');
		// $c->content_tag			= '';
		 $c->created_at				= date('Y-m-d H:i:s');
		 $c->updated_at				= date('Y-m-d H:i:s');
		 $c->created_by				= Auth::user()->id;
		 $c->content_url			= $url;
		 $c->save();
		 $cid = Content::where(array(
		 	'content_url'	=> $url,
		 	'created_by'	=> Auth::user()->id
		 	))->first();
		 $t = new Tagcontent;
		 if(!empty($tag)){
       
        foreach($tag as $tags =>$tt):
          DB::table('tb_tagcontent')->insert(array(
              'tag_id'  => $tt,
              'content_id' => $cid->id,
               'tag_count' =>'0'
            ));
        endforeach;
      }
      $filenum = Uploadfiles::where('token',$token )->count();
      if($filenum > 0){
      }else{
      	$file_type = '0';
		  			$Images 	= new Uploadfiles;
    		 
    			$Images 	->files_newname 		="";
    			$Images 	->files_oldname			="";
    			$Images 	->files_type			=$file_type;
    			$Images 	->token 				= Input::get('key');
    			$Images 	->save();
      }
      
	return Redirect::to('backend/content')->with(
				array(
					'save-success' => 'save'
				       ));

	}

	public function postEdit()
	{
		$url = Helpers::create_url(Input::get('txt_name'));
		 $tag = Input::get('txt_tag');
		 $id = Input::get('id');
	 
		 $c = Content::find($id);
		 $c->content_name 			= Input::get('txt_name');
		 $c->content_categories		= Input::get('txt_type');
		 $c->content_detail			= Input::get('txt_detail');
		 $c->content_author			= Input::get('txt_author');
		 $c->content_year			= Input::get('txt_year');
		// $c->content_file			= Input::get('key');
		// $c->content_view			= '0';
		// $c->content_tag			= '';
		// $c->created_at				= date('Y-m-d H:i:s');
		 $c->updated_at				= date('Y-m-d H:i:s');
	//	 $c->created_by				= Auth::user()->id;
		 $c->content_url			= $url;
		 $c->save();
		 
		 $t = new Tagcontent;
		 if(!empty($tag)){
       	
        foreach($tag as $tags =>$tt):
        	$num = Tagcontent::where(array('tag_id'=>$tt,'content_id'=>$id))->count();
        	  if($num <= 0){
          		DB::table('tb_tagcontent')->insert(array(
              'tag_id'  => $tt,
              'content_id' => $id,
               'tag_count' =>'0'
            ));
       
      			}else{
      				Tagcontent::where('content_id','=',$id)
      				->where('tag_id','!=',$tt)->delete();
      			}
        endforeach;
      }
   
      return Redirect::to('backend/content')->with(
				array(
					'edit-success' => 'edit'
				       ));

	}


	public function postAddfile($key)
	{

		  		
		  		if (Input::hasFile('uploadfile')){

		  			$filename = Input::file('uploadfile')->getClientOriginalName();
		  			 $file= Input::file('uploadfile');
		  			$files_size= File::size($file);
		  		$img_name = Str::random(16,'numberic').".".Input::file('uploadfile')->getClientOriginalExtension();
		  		$file_type = Input::file('uploadfile')->getClientOriginalExtension();
				$Path='uploadfiles'; 
    			$upload_success=	Input::file('uploadfile')->move($Path,$img_name);

    			$Images 	= new Uploadfiles;
    		 
    			$Images 	->files_newname 		=$img_name;
    			$Images 	->files_oldname			=$filename;
    			$Images 	->files_type			=$file_type;
    			$Images		->files_size			=$files_size;
    			$Images 	->token 				=$key;
    			$Images 	->save();

		  			

		  		}

	}
	public function postEditfile($key)
	{

		  		$oldfile = Uploadfiles::where('token',$key)->first();

		  		if (Input::hasFile('uploadfile')){

		  			if($oldfile->files_type=="0") {

					//	 $img = Uploadfiles::where('token',$key)->delete();
						}else{
						// $img = Uploadfiles::where('token',$key)->delete();
						 $filename='uploadfiles/'.$oldfile->files_newname; 
						 File::delete($filename);
						}

		  		$filename = Input::file('uploadfile')->getClientOriginalName();
		  		$file= Input::file('uploadfile');
		  			$files_size= File::size($file);
		  		$img_name = Str::random(16,'numberic').".".Input::file('uploadfile')->getClientOriginalExtension();
		  		$file_type = Input::file('uploadfile')->getClientOriginalExtension();
				$Path='uploadfiles'; 
    			$upload_success=	Input::file('uploadfile')->move($Path,$img_name);

    			 DB::table('tb_files')
	            ->where('token', $key)
	            ->update(array(
	    		 	'files_newname'			=>$img_name,
	    			'files_oldname'			=>$filename,
	    			'files_type'			=>$file_type,
	    			'files_size'			=>$files_size
	    			
	    		));
    			 }

	}

	public function postDropimages($key){

			 $img = Uploadfiles::where('token','=',$key)->first();
		 	 $filename='uploadfiles/'.$img->files_newname; 
			 File::delete($filename);
			 $img = Uploadfiles::where('token',$key)->delete();
			 
		}
}