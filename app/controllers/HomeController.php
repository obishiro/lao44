<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function index()
	{
		$Enviroment = Enviroment::first();
		$Categories = Categories::all();
		$Categories_index = Categories::where('categories_show','1')->orderBy('id','desc')->get();
		$Mainmenu = Mainmenu::orderBy('mainmenu_sorting','asc')->get();
		$DataVdo = Uploadfiles::select('tb_files.files_newname','tb_files.files_oldname','tb_content.id'
				,'tb_content.content_name','tb_content.content_view','tb_content.content_url')
				->where('tb_files.files_type','mp4')->orWhere(function($sql){
				$sql ->orWhere('tb_files.files_type','=','MP4')
				->orWhere('tb_files.files_type','=','wmv')
				->orWhere('tb_files.files_type','=','WMV')
				->orWhere('tb_files.files_type','=','avi')
				->orWhere('tb_files.files_type','=','AVI')
				->orWhere('tb_files.files_type','=','mkv')
				->orWhere('tb_files.files_type','=','MKV')
				->orWhere('tb_files.files_type','=','mov')
				->orWhere('tb_files.files_type','=','MOV');
		})
		->join('tb_content','tb_content.content_file','=','tb_files.token')
		->orderBy('tb_content.id','desc')
		->take(6)->skip(0)
		->get();
		// $DataRecome = Uploadfiles::select('tb_files.files_type'
  //       ,'tb_content.content_name','tb_content.id'
  //       ,'tb_content.created_by','tb_content.created_at','tb_files.files_size','tb_content.content_view',
  //       'tb_content.content_url','tb_categories.categories_name')
  //       ->where('tb_content.content_show','=','1')
  //       ->join('tb_categories','tb_categories.id','=','tb_content.content_categories')
  //       ->join('tb_tagcontent','tb_tagcontent.content_id','=','tb_content.id')
  //       ->join('tb_content','tb_content.content_file','=','tb_files.token')
        
  //       ->groupBy('tb_content.id')
  //       ->orderBy('tb_content.id','desc')
  //       ->take(6)->skip(0)
		// ->get();

		$DataRecome = Uploadfiles::select(['tb_files.files_type'
        ,'tb_content.content_name','tb_content.id'
        ,'tb_content.created_by','tb_content.created_at','tb_files.files_size','tb_content.content_view',
        'tb_content.content_url','tb_categories.categories_name'
        ])
        ->where('tb_content.content_show','=','1')
       
        ->join('tb_content','tb_content.content_file','=','tb_files.token')
        ->join('tb_categories','tb_categories.id','=','tb_content.content_categories')
        ->join('tb_tagcontent','tb_tagcontent.content_id','=','tb_content.id')
        ->groupBy('tb_content.id')
        ->orderBy('tb_content.id','desc')
         ->groupBy('tb_content.id')
        ->orderBy('tb_content.id','desc')
        ->take(6)->skip(0)->get();
		

		return View::make('frontend.index')->with(array(
			'env'		=> $Enviroment,
			'Categories'	=> $Categories,
			'Mainmenu'	=> $Mainmenu,
			'DataVdo'	=> $DataVdo,
			'DataRecome'	=> $DataRecome,
			'number'	=> 1,
			'Categories_index'	=>$Categories_index

			));
	}

          
}
