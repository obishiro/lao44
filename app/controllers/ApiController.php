<?php

class ApiController extends BaseController {

	public function getDatacategories ()
	{
 	
		$sql = Categories::select([
			 
			'id','categories_name','categories_url','categories_show','created_at','updated_at'])
		->orderBy('id','desc');
		$datatables = Datatables::of($sql)
		->removeColumn('id')
		->addColumn('no','')
          ->editColumn('categories_name','<a href="{{ URL::to(\'categories\',array($id,$categories_url))}}" target="_blank">{{$categories_name}}</a> ')
          ->editColumn('categories_show','@if($categories_show==1)  <a href=\'{{ URL::to(\'backend/unshow/categories\',array($id))}}\'><i class="fa fa-check-circle fa-2x"></i></a>@else  <a href=\'{{ URL::to(\'backend/show/categories\',array($id))}}\'><i class="fa fa-times-circle fa-2x"></i></i></a> @endif')
          ->removeColumn('categories_url')
		->addColumn('tools','
			<a href="#" class="btn btn-info" data-toggle="modal" data-target="#Modal-add-{{$id}}">
			<i class="fa fa-pencil"></i>
			 {{ Lang::get(\'msg.msg_edit\',array(),\'th\')}}
			</a>
			<a href="{{ URL::to(\'backend/del/categories\',array($id)) }}" class="btn btn-danger" onclick="javascript:return confirm(\'{{ Lang::get(\'msg.msg_confirm\',array(),\'th\') }}\')" >
			<i class="fa fa-trash"></i>
			 {{ Lang::get(\'msg.msg_del\',array(),\'th\')}}
			</a>
			<div class="modal fade" id="Modal-add-{{$id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                          <div class="modal-dialog">
                                            <div class="modal-content">
                                              <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel">
                                                 <i class="fa fa-pencil"></i> {{ Lang::get(\'msg.msg_edit\',array(),\'th\') }}                                         
                                                     
                                                </h4>
                                              </div>
                                              <div class="modal-body" style="border-bottom:0px">
                                              	 
                                              	{{ Form::open(array(
                                              		\'method\'=>\'POST\',\'name\'=>\'editform\'
                                              		,\'url\'=>\'backend/edit/categories\'
                                              		 
                                              		))}}
                                              	 
                                              	{{Form::label(\'label\',Lang::get(\'msg.categories_name\', array(), \'th\'))}}
                                              	{{Form::input(\'text\', \'txt_name\', $categories_name, 
                                              	array(
                                              		\'class\'=>\'form-control col-xs-12\',
                                              		\'style\'=>\'width:100%;margin-bottom:15px\'

                                              		))}}
                                              	 
												
                                              	 <div class="modal-footer"  style="border:0px">
                                                
                                               <button type="button" class="btn btn-danger" data-dismiss="modal">
                                               	<i class="fa fa-close"></i>
                                                {{ Lang::get(\'msg.msg_cancle\',array(), \'th\')}}
                                            </button>
                                            <button type="submit" class="btn btn-primary">
                                            	<i class="fa fa-check-circle"></i>
                                                {{ Lang::get(\'msg.msg_submit\',array(), \'th\')}}
                                            </button>
                                            {{Form::hidden("id",$id)}}
                                            {{ Form::close()}}
                                              </div>
                                                
                                            </div>
                                              </div>

                                          </div>
                                        </div>
			');
		 

        return $datatables->make(true);
		 
		
		 
	}
  public function getDatatag ()
  {
  
    $sql = Tag::select([
       
      'id','tag_name','created_at','updated_at'])
    ->orderBy('id','desc');
    $datatables = Datatables::of($sql)
    ->removeColumn('id')
    ->addColumn('no','')
    ->addColumn('tools','
      <a href="#" class="btn btn-info" data-toggle="modal" data-target="#Modal-add-{{$id}}">
      <i class="fa fa-pencil"></i>
       {{ Lang::get(\'msg.msg_edit\',array(),\'th\')}}
      </a>
      <a href="{{ URL::to(\'backend/del/tag\',array($id)) }}" class="btn btn-danger" onclick="javascript:return confirm(\'{{ Lang::get(\'msg.msg_confirm\',array(),\'th\') }}\')" >
      <i class="fa fa-trash"></i>
       {{ Lang::get(\'msg.msg_del\',array(),\'th\')}}
      </a>
      <div class="modal fade" id="Modal-add-{{$id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                          <div class="modal-dialog">
                                            <div class="modal-content">
                                              <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel">
                                                 <i class="fa fa-pencil"></i> {{ Lang::get(\'msg.msg_edit\',array(),\'th\') }}                                         
                                                     
                                                </h4>
                                              </div>
                                              <div class="modal-body" style="border-bottom:0px">
                                                 
                                                {{ Form::open(array(
                                                  \'method\'=>\'POST\',\'name\'=>\'editform\'
                                                  ,\'url\'=>\'backend/edit/tag\'
                                                   
                                                  ))}}
                                                 
                                                {{Form::label(\'label\',Lang::get(\'msg.tag_name\', array(), \'th\'))}}
                                                {{Form::input(\'text\', \'txt_name\', $tag_name, 
                                                array(
                                                  \'class\'=>\'form-control col-xs-12\',
                                                  \'style\'=>\'width:100%;margin-bottom:15px\'

                                                  ))}}
                                                 
                        
                                                 <div class="modal-footer"  style="border:0px">
                                                
                                               <button type="button" class="btn btn-danger" data-dismiss="modal">
                                                <i class="fa fa-close"></i>
                                                {{ Lang::get(\'msg.msg_cancle\',array(), \'th\')}}
                                            </button>
                                            <button type="submit" class="btn btn-primary">
                                              <i class="fa fa-check-circle"></i>
                                                {{ Lang::get(\'msg.msg_submit\',array(), \'th\')}}
                                            </button>
                                            {{Form::hidden("id",$id)}}
                                            {{ Form::close()}}
                                              </div>
                                                
                                            </div>
                                              </div>

                                          </div>
                                        </div>
      ');
     

        return $datatables->make(true);
     
    
     
  }
  public function getDatamainmenu ()
  {
  
    $sql = Mainmenu::select([
       
      'id','mainmenu_name','mainmenu_type','m_url','created_at','updated_at'])
    ->orderBy('id','desc');
    $datatables = Datatables::of($sql)
    ->removeColumn('id')
    ->removeColumn('m_url')
    ->editColumn('mainmenu_name','@if($mainmenu_type==1) <a href="{{ URL::to(\'mainmenu\',array($id,$m_url))}}">{{$mainmenu_name}}</a> @else {{$mainmenu_name}} @endif ')
    ->editColumn('mainmenu_type','{{ Helpers::mainmenutype($mainmenu_type) }}')
    ->addColumn('no','')
    ->addColumn('tools','
      <a href="{{ URL::to(\'backend/menu/edit/mainmenu\',array($id)) }}" class="btn btn-info">
      <i class="fa fa-pencil"></i>
       {{ Lang::get(\'msg.msg_edit\',array(),\'th\')}}
      </a>
      <a href="{{ URL::to(\'backend/del/mainmenu\',array($id)) }}" class="btn btn-danger" onclick="javascript:return confirm(\'{{ Lang::get(\'msg.msg_confirm\',array(),\'th\') }}\')" >
      <i class="fa fa-trash"></i>
       {{ Lang::get(\'msg.msg_del\',array(),\'th\')}}
      </a>
       
      ');
     

        return $datatables->make(true);
     
    
     
  }
  public function getDatasubmenu ()
  {
  
    $sql = Submenu::select([
       
      'tb_submenu.id','tb_submenu.submenu_name','tb_mainmenu.mainmenu_name'
      ,'tb_submenu.submenu_type','tb_submenu.updated_at'
      ])
    ->join('tb_mainmenu','tb_mainmenu.id','=','tb_submenu.submenu_categories')
    ->orderBy('tb_submenu.id','desc');
    $datatables = Datatables::of($sql)
    ->removeColumn('id')
  //  ->editColumn('submenu_categories','{{ Helpers::mainmenutype($submenu_type) }}')
    ->editColumn('submenu_type','{{ Helpers::mainmenutype($submenu_type) }}')
    ->addColumn('no','')
    ->addColumn('tools','
      <a href="{{ URL::to(\'backend/menu/edit/submenu\',array($id)) }}" class="btn btn-info">
      <i class="fa fa-pencil"></i>
       {{ Lang::get(\'msg.msg_edit\',array(),\'th\')}}
      </a>
      <a href="{{ URL::to(\'backend/del/submenu\',array($id)) }}" class="btn btn-danger" onclick="javascript:return confirm(\'{{ Lang::get(\'msg.msg_confirm\',array(),\'th\') }}\')" >
      <i class="fa fa-trash"></i>
       {{ Lang::get(\'msg.msg_del\',array(),\'th\')}}
      </a>
       
      ');
             return $datatables->make(true);
      }

    public function getDatacontent ()
  {
  
    $sql = Content::select([
       
      'tb_content.id','tb_content.content_name','tb_categories.categories_name'
      ,'tb_content.content_categories','tb_files.files_type','tb_content.updated_at','tb_content.content_view','tb_content.content_url','tb_content.content_show'

      ])
    ->join('tb_categories','tb_categories.id','=','tb_content.content_categories')
    ->join('tb_files','tb_files.token','=','tb_content.content_file')
    ->orderBy('tb_content.id','desc');
    $datatables = Datatables::of($sql)
    ->removeColumn('id')
    ->removeColumn('content_url')
    ->editColumn('content_name','<a href="{{ URL::to(\'content\',array($id,$content_url))}}" target="_blank">{{$content_name}}</a> ')
    ->editColumn('files_type','{{ Helpers::filestype($files_type) }}')
    ->editColumn('content_categories','{{$categories_name}}')
    ->editColumn('content_view','{{ number_format($content_view) }}')
    ->editColumn('content_show','@if($content_show==1)  <a href=\'{{ URL::to(\'backend/unshow/content\',array($id))}}\'><i class="fa fa-check-circle fa-2x"></i></a>@else  <a href=\'{{ URL::to(\'backend/show/content\',array($id))}}\'><i class="fa fa-times-circle fa-2x"></i></i></a> @endif')

    ->addColumn('no','')
    ->addColumn('tools','
      
      <a href="{{ URL::to(\'backend/content/editcontent\',array($id)) }}" class="btn btn-info">
      <i class="fa fa-pencil"></i>
       {{ Lang::get(\'msg.msg_edit\',array(),\'th\')}}
      </a>
      <a href="{{ URL::to(\'backend/del/content\',array($id)) }}" class="btn btn-danger" onclick="javascript:return confirm(\'{{ Lang::get(\'msg.msg_confirm\',array(),\'th\') }}\')" >
      <i class="fa fa-trash"></i>
       {{ Lang::get(\'msg.msg_del\',array(),\'th\')}}
      </a>
       
      ');
             return $datatables->make(true);
      }

       public function getDataviewcategories($id,$url)
      {
       $sql = Uploadfiles::select(['tb_files.files_type'
        ,'tb_content.content_name','tb_content.id'
        ,'tb_content.created_by','tb_content.created_at','tb_files.files_size','tb_content.content_view',
        'tb_content.content_url'
        ])
        ->where('tb_categories.id','=',$id)->where('tb_categories.categories_url','=',$url)
    ->join('tb_content','tb_content.content_file','=','tb_files.token')
    ->join('tb_categories','tb_categories.id','=','tb_content.content_categories')
    ->orderBy('tb_content.id','desc');
    $datatables = Datatables::of($sql)
    ->addColumn('no','')
    ->removeColumn('id')
    ->editColumn('created_by','Administrator')
    ->editColumn('content_name','<a href="{{ URL::to("content",array($id,$content_url))}}">{{ $content_name }}</a>')
    ->editColumn('content_view','{{ number_format($content_view) }}')
    ->editColumn('files_type','{{ Helpers::filestype($files_type) }}')
    ->editColumn('files_size','{{number_format($files_size/1024)}} kb.')
    ->removeColumn('content_url');
    return $datatables->make(true);
      }
       public function getDataviewtag($id,$url)
      {
       $sql = Uploadfiles::select(['tb_files.files_type'
        ,'tb_content.content_name','tb_content.id'
        ,'tb_content.created_by','tb_content.created_at','tb_files.files_size','tb_content.content_view',
        'tb_content.content_url'
        ])
        ->where('tb_tagcontent.tag_id','=',$id)
        ->join('tb_content','tb_content.content_file','=','tb_files.token')
       // ->join('tb_categories','tb_categories.id','=','tb_content.content_categories')
        ->join('tb_tagcontent','tb_tagcontent.content_id','=','tb_content.id')
        ->orderBy('tb_content.id','desc');
    $datatables = Datatables::of($sql)
    ->addColumn('no','')
    ->removeColumn('id')
    ->editColumn('created_by','Administrator')
    ->editColumn('content_name','<a href="{{ URL::to("content",array($id,$content_url))}}">{{ $content_name }}</a>')
    ->editColumn('content_view','{{ number_format($content_view) }}')
    ->editColumn('files_type','{{ Helpers::filestype($files_type) }}')
    ->editColumn('files_size','{{number_format($files_size/1024)}} kb.')
    ->removeColumn('content_url');
    return $datatables->make(true);
      }

       public function getDataviewsearch($text)
      {
       $sql = Uploadfiles::select(['tb_files.files_type'
        ,'tb_content.content_name','tb_content.id'
        ,'tb_content.created_by','tb_content.created_at','tb_files.files_size','tb_content.content_view',
        'tb_content.content_url','tb_categories.categories_name'
        ])
        ->where('tb_content.content_name','like','%'.$text.'%')
        ->orWhere('tb_categories.categories_name','like','%'.$text.'%')
        ->join('tb_content','tb_content.content_file','=','tb_files.token')
        ->join('tb_categories','tb_categories.id','=','tb_content.content_categories')
        ->join('tb_tagcontent','tb_tagcontent.content_id','=','tb_content.id')
        ->groupBy('tb_content.id')
        ->orderBy('tb_content.id','desc');
    $datatables = Datatables::of($sql)
    ->addColumn('no','')
    ->removeColumn('id')
    ->editColumn('created_by','Administrator')
    ->editColumn('content_name','<a href="{{ URL::to("content",array($id,$content_url))}}">{{ $content_name }}</a>')
    ->editColumn('content_view','{{ number_format($content_view) }}')
    ->editColumn('files_type','{{ Helpers::filestype($files_type) }}')
    ->editColumn('files_size','{{number_format($files_size/1024)}} kb.')
    ->removeColumn('content_url');
    return $datatables->make(true);
      }


}