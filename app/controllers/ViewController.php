<?php

class ViewController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /view
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}
	public function getViewmainmenu($id,$url)
	{
		  	$sql = Mainmenu::where(array('id'=>$id,'m_url'=>$url))->first();
		  	$Enviroment = Enviroment::first();
			$Categories = Categories::all();
			$Mainmenu = Mainmenu::orderBy('mainmenu_sorting','asc')->get();
		  return View::make('viewmodule.mainmenu')->with(
             array(
             		'data' 			=>$sql,
             		'env'			=> $Enviroment,
             		'Mainmenu'		=> $Mainmenu,
					'Categories'	=> $Categories
             	));
	}
	public function getViewsubmenu($id,$url)
	{
		  	$sql = Submenu::where(array('id'=>$id,'s_url'=>$url))->first();
		  	$Enviroment = Enviroment::first();
			$Categories = Categories::all();
			$Mainmenu = Mainmenu::orderBy('mainmenu_sorting','asc')->get();
		  return View::make('viewmodule.submenu')->with(
             array(
             		'data' 			=>$sql,
             		'env'			=> $Enviroment,
             		'Mainmenu'		=> $Mainmenu,
					'Categories'	=> $Categories
             	));
	}
	public function getViewcontent($id,$url)
	{
		 $sql = Uploadfiles::select('tb_files.files_type','tb_files.files_newname'
        ,'tb_content.content_name','tb_content.id'
        ,'tb_content.created_by','tb_content.created_at','tb_files.files_size','tb_content.content_view',
        'tb_content.content_url','content_detail','tb_categories.categories_name','tb_categories.id as cid','tb_categories.categories_url'
        )
        ->where('tb_content.id','=',$id)->where('tb_content.content_url','=',$url)
	    ->join('tb_content','tb_content.content_file','=','tb_files.token')
	    ->join('tb_categories','tb_categories.id','=','tb_content.content_categories')
	    ->first();
		  	$Enviroment = Enviroment::first();
			$Categories = Categories::all();
			$Mainmenu = Mainmenu::orderBy('mainmenu_sorting','asc')->get();
			$tag = Tagcontent::select('tb_tag.tag_name','tb_tag.id as tagid','tb_tag.tag_url')
			->join('tb_tag','tb_tag.id','=','tb_tagcontent.tag_id')
			->where('tb_tagcontent.content_id',$id)->get();
			$updateview = Content::find($id);
			$oldview = $updateview->content_view;
			$updateview->content_view = $oldview+1;
		 	$updateview->save();

			// $fsize = $sql->files_size;
			// $files_size = $fsize/1024;
		 
		  return View::make('viewmodule.content')->with(
             array(
             		'data' 			=>$sql,
             		'env'			=> $Enviroment,
             		'Mainmenu'	=> $Mainmenu,
					'Categories'	=> $Categories,
					'tags'				=>$tag
					//'size'			=>$files_size
             	));
	}
	public function getViewvdo($id,$url)
	{
		 $sql = Uploadfiles::select('tb_files.files_type','tb_files.files_newname'
        ,'tb_content.content_name','tb_content.id'
        ,'tb_content.created_by','tb_content.created_at','tb_files.files_size','tb_content.content_view',
        'tb_content.content_url','content_detail','tb_categories.categories_name','tb_categories.id as cid','tb_categories.categories_url'
        )
        ->where('tb_content.id','=',$id)->where('tb_content.content_url','=',$url)
	    ->join('tb_content','tb_content.content_file','=','tb_files.token')
	    ->join('tb_categories','tb_categories.id','=','tb_content.content_categories')
	    ->first();
		  	$Enviroment = Enviroment::first();
			$Categories = Categories::all();
			$Mainmenu = Mainmenu::orderBy('mainmenu_sorting','asc')->get();
			$tag = Tagcontent::select('tb_tag.tag_name','tb_tag.id as tagid','tb_tag.tag_url')
			->join('tb_tag','tb_tag.id','=','tb_tagcontent.tag_id')
			->where('tb_tagcontent.content_id',$id)->get();
			$updateview = Content::find($id);
			$oldview = $updateview->content_view;
			$updateview->content_view = $oldview+1;
	    		$updateview->save();
	  //   		$fsize = $sql->files_size;
			// $files_size = $fsize/1024;
		 
		  return View::make('viewmodule.vdo')->with(
             array(
             		'data' 			=>$sql,
             		'env'			=> $Enviroment,
             		'Mainmenu'	=> $Mainmenu,
					'Categories'	=> $Categories,
					'tags'				=>$tag
					//'size'			=>$files_size
             	));
	}
	public function getViewcategories($id,$url)
	{
		  	$sql = Mainmenu::where(array('id'=>$id,'m_url'=>$url))->get();
		  	$Enviroment = Enviroment::first();
			$Categories = Categories::all();
			$Mainmenu = Mainmenu::orderBy('mainmenu_sorting','asc')->get();
			$cat = Categories::find($id);
			$title = $cat->categories_name;
			$api = URL::to('data/viewcategories',array($id,$url));
			
		 
		  return View::make('viewmodule.categories')->with(
             array(
             		'data' 			=>$sql,
             		'env'			=> $Enviroment,
             		'Mainmenu'	=> $Mainmenu,
					'Categories'	=> $Categories,
					'api'		=> $api,
					'c'				=> $cat,
					'title'			=> $title,
					'number'		=> 1
             	));
	}
	public function getViewtag($id,$url)
	{
		  	$sql = Tag::where(array('id'=>$id,'tag_url'=>$url))->first();
		  	$Enviroment = Enviroment::first();
			$Categories = Categories::all();
			$Mainmenu = Mainmenu::orderBy('mainmenu_sorting','asc')->get();
			$cat = Categories::find($id);
		//	$title = $sql->tag_name;
			$api = URL::to('data/viewtag',array($id,$url));
			$updateview = Tag::find($id);
			$oldview = $updateview->tag_count;
			$updateview->tag_count = $oldview+1;
	    	$updateview->save();
			
		 
		  return View::make('viewmodule.tag')->with(
             array(
             		'data' 			=>$sql,
             		'env'			=> $Enviroment,
             		'Mainmenu'		=> $Mainmenu,
					'Categories'	=> $Categories,
					'api'			=> $api,
					'c'				=> $sql,
					 
					'number'		=> 1
             	));
	}
	public function postViewsearch()
	{
			$text = Input::get('keyword');
			
		  //	$sql = Content::where(array('id'=>$id,'tag_url'=>$url))->first();
		  	$Enviroment = Enviroment::first();
			$Categories = Categories::all();
			$Mainmenu = Mainmenu::orderBy('mainmenu_sorting','asc')->get();
		//	$cat = Categories::find($id);
			
			$api = URL::to('data/viewsearch',array($text));
		 	
		 
		  return View::make('viewmodule.search')->with(
             array(
             	 
             		'env'			=> $Enviroment,
             		'Mainmenu'		=> $Mainmenu,
					'Categories'	=> $Categories,
					'api'			=> $api,
					 'title'		=> $text,
					 
					'number'		=> 1
             	));
	}

	 

}