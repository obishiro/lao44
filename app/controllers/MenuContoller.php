<?php

class MenuContoller extends BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /menucontoller
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}
	public function getMenu($type)
	{
		switch ($type):
			case 'mainmenu':
			$title = Lang::get('msg.mainmenu',array(),'th');
			$sql = Mainmenu::orderBy('id','desc')->get();
			 
			$api = URL::to('backend/data/mainmenu');
			return View::make('backend.menu.mainmenu')->with(
				 array(
				 	'title' 	=>$title,
				 
				 	'api'		=> $api,
				 	'status'	=> 'null'
				       ));

			break;
			case 'submenu':
			$title = Lang::get('msg.submenu',array(),'th');
			$api = URL::to('backend/data/submenu');
			return View::make('backend.menu.submenu')->with(
				 array(
				 	'title' 	=>$title,
				  	'api'	=> $api,
				 	'status'	=> 'null'
				       ));

				break;
		endswitch;
	}

	public function getAdd($type)
	{
		switch($type):
			case 'mainmenu':
				$title = Lang::get('msg.mainmenu',array(),'th');
				$sql = Mainmenu::orderBy('id','desc')->get();
				$rules = ['txt_name'=>'required','txt_type'=>'required'];
				 
				return View::make('backend.menu.addmainmenu')->with(
				 array(
				 	'title' 	=>$title,
				 	'rules'		=>$rules,
				  
				 	'status'	=> 'null'
				       ));
			break;
			case 'submenu':
				$title = Lang::get('msg.submenu',array(),'th');
				 $sql = Mainmenu::where('mainmenu_type','3')->orderBy('id','desc')->get();
			 
				$rules = ['txt_name'=>'required','txt_type'=>'required'];
				 
				return View::make('backend.menu.addsubmenu')->with(
				 array(
				 	'title' 	=>$title,
				 	'rules'	=>$rules,
				  	'sql'	=> $sql,
				 	'status'	=> 'null'
				       ));
			break;

		endswitch;
	}
	public function postAdd($type)
	{
		switch($type):
			case 'mainmenu':
				$url = Helpers::create_url(Input::get('txt_name'));
				 $m = new Mainmenu;
				 $m->mainmenu_name = Input::get('txt_name') ;
				 $m->mainmenu_type = Input::get('txt_type');
				 $m->mainmenu_detail = Input::get('txt_detail');
				 $m->mainmenu_url = Input::get('txt_url');
				 $m->m_url 			= $url;
				 $m->created_at = date('Y-m-d H:i:s');
				 $m->updated_at = date('Y-m-d H:i:s');
				 $m->create_by = Auth::user()->id;
				 $m->save();
				return Redirect::to('backend/menu/add/mainmenu')->with(
				array(
					'save-success' => 'save'
				       ));
			break;
			case 'submenu':
				$url = Helpers::create_url(Input::get('txt_name'));
				 $m = new Submenu;
				 $m->submenu_name = Input::get('txt_name') ;
				 $m->submenu_categories = Input::get('txt_categories');
				 $m->submenu_type = Input::get('txt_type');
				 $m->submenu_detail = Input::get('txt_detail');
				 $m->submenu_url = Input::get('txt_url');
				 $m->s_url = $url;
				 $m->created_at = date('Y-m-d H:i:s');
				 $m->updated_at = date('Y-m-d H:i:s');
				 $m->create_by = Auth::user()->id;
				 $m->save();
				return Redirect::to('backend/menu/add/submenu')->with(
				array(
					'save-success' => 'save'
				       ));
			break;

		endswitch;
	}
	public function getEdit($type,$id)
	{
		switch($type):
			case 'mainmenu': 
				$title = Lang::get('msg.msg_edit',array(),'th').' '.Lang::get('msg.mainmenu',array(),'th');
				$m = Mainmenu::find($id);
				$rules = ['txt_name'=>'required','txt_type'=>'required'];
				return View::make('backend.menu.editmainmenu')->with(
					array(
					'title' 	=>$title,
                                                    'm' => $m,
                                                    'rules' => $rules
					));
			break;

			case 'submenu': 
				$title = Lang::get('msg.msg_edit',array(),'th').' '.Lang::get('msg.submenu',array(),'th');
				$m = Submenu::find($id);
				$sql = Mainmenu::where('mainmenu_type','3')->orderBy('id','desc')->get();
				$rules = ['txt_name'=>'required','txt_type'=>'required'];
				return View::make('backend.menu.editsubmenu')->with(
					array(
					'title' 	=>$title,
                                                    'm' 	=> $m,
                                                    'sql'	=> $sql,
                                                    'rules' 	=> $rules
					));
			break;

		endswitch;	
	}

	public function postEdit($type)
	{
		   $id = Input::get('id');
		 switch($type):
			case 'mainmenu': 
				$m = Mainmenu::find($id);
				$url = Helpers::create_url(Input::get('txt_name'));
				$m->mainmenu_name = Input::get('txt_name') ;
				 $m->mainmenu_type = Input::get('txt_type');
				 $m->mainmenu_detail = Input::get('txt_detail');
				 $m->mainmenu_url = Input::get('txt_url');
				 $m->updated_at = date('Y-m-d H:i:s');
				 $m->m_url = $url;
				 $m->save();
				 return Redirect::to('backend/menu/mainmenu')->with(
				array(
					'edit-success' => 'edit'
				       ));
			break;
			case 'submenu': 
				$m = Submenu::find($id);
				$url = Helpers::create_url(Input::get('txt_name'));
				$m->submenu_name = Input::get('txt_name') ;
				 $m->submenu_categories = Input::get('txt_categories');
				 $m->submenu_type = Input::get('txt_type');
				 $m->submenu_detail = Input::get('txt_detail');
				 $m->submenu_url = Input::get('txt_url');
				 $m->updated_at = date('Y-m-d H:i:s');
				 $m->s_url = $url;
				 $m->save();
				 return Redirect::to('backend/menu/submenu')->with(
				array(
					'edit-success' => 'edit'
				       ));
			break;
		 endswitch;
	}
	 

	

}