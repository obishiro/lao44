<?php

class BackendController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /backend
	 *
	 * @return Response
	 */
	public function index()
	{
		return View::make('backend.content');
	}

	 
	public function postLogin()
		{   
     		 
    	 	$rules = array(
			'username'	=> 'required',
			'password'	=> 'required'

								);
				$validator = Validator::make(Input::all(), $rules);

    		if ($validator->fails())
    			{
       		 		return Redirect::to('login')->withErrors($validator)->withInput();
     			}else{
 
		       if(Auth::attempt(array('username'=>Input::get('username'),'password'=>Input::get('password')))) {
		          
		       			switch(Auth::user()->user_status):
		       				case '0':
		       					return Redirect::to('login');
		       				break;
		       				case '1':
		       					return Redirect::to('backend/content');
		       				break;
		       			endswitch;
		       
		            }else{
				        return Redirect::to('login')
				        ->with('error_login_incorrect',Lang::get('msg.error_login_incorrect',array(),'th'));
				      }
				  }
			  } 

public function getShow($type,$id)
{
	switch ($type) {
		case 'categories':
			$c= Categories::find($id);
			$c->categories_show = 1;
			$c->save();
			return Redirect::action('BackendController@getConfig',array($type));
			break;
		case 'content':
			$c= Content::find($id);
			$c->content_show = 1;
			$c->save();
			return Redirect::to('backend/content');
			break;
		
		 
		
	}
		
}
public function getUnshow($type,$id)
{
	switch ($type) {
		case 'categories':
			$c= Categories::find($id);
			$c->categories_show = 0;
			$c->save();
			return Redirect::action('BackendController@getConfig',array($type));
			break;
		case 'content':
			$c= Content::find($id);
			$c->content_show = 0;
			$c->save();
			return Redirect::to('backend/content');
			break;
		
		 

	}
	
	 
}
public function getConfig($type)
{

	switch($type):
		case 'enviroment':
			$title= Lang::get('msg.config-env',array(),'th');
			//$rows = Enviroment::where('user_id',Auth::user()->id)->count();
			 
			$sql =Enviroment::first();
			
			return View::make('backend.config.enviroment')->with(
				 array(
				 	'title' =>$title,
				 	'data'	=>$sql,
				 	'rules' =>''
				       ));
		break;
		case 'password':
			$title= Lang::get('msg.config-password',array(),'th');
			$sql= User::find(Auth::user()->id);
			return View::make('backend.config.password')->with(
				 array(
				 	'title' =>$title,
				 	'data'	=>$sql,
				 	'rules' =>''
				       ));
		break;
		case 'categories':
			$title = Lang::get('msg.config-categories',array(),'th');
			$sql = Categories::orderBy('id','desc')->get();
			$rules = ['txt_name'=>'required'];
			$api = URL::to('backend/data/categories');
			return View::make('backend.config.categories')->with(
				 array(
				 	'title' 	=>$title,
				 	'rules'		=>$rules,
				 	'api'		=> $api,
				 	'status'	=> 'null'
				       ));

		break;
		case 'tag':
			$title = Lang::get('msg.config-tag',array(),'th');
			$sql = Tag::orderBy('id','desc')->get();
			$rules = ['txt_name'=>'required','txt_type'=>'required'];
			$api = URL::to('backend/data/tag');
			return View::make('backend.config.tag')->with(
				 array(
				 	'title' 	=>$title,
				 	'rules'		=>$rules,
				 	'api'		=> $api,
				 	'status'	=> 'null'
				       ));

		break;
		case 'mainmenu':
			$title = Lang::get('msg.mainmenu',array(),'th');
			$sql = Mainmenu::orderBy('id','desc')->get();
			$rules = ['txt_name'=>'required'];
			$api = URL::to('backend/data/mainmenu');
			return View::make('backend.config.mainmenu')->with(
				 array(
				 	'title' 	=>$title,
				 	'rules'		=>$rules,
				 	'api'		=> $api,
				 	'status'	=> 'null'
				       ));

		break;
	endswitch;


}
public function postConfig($type)
{
	switch($type):
		case 'enviroment':
			DB::table('tb_enviroment')
			->where('created_by',Auth::user()->id)
			->update(
                 array(
                 	'web_name_lo'	=> Input::get('web_name_lo'),
                 	'web_name_en'	=> Input::get('web_name_en'),
                 	'web_address'	=> Input::get('web_address'),
                 	'web_tel'		=> Input::get('web_tel'),
                 	'web_email'	=> Input::get('web_email'),
                 	'web_keyword'	=> Input::get('web_keyword'),
                 	'web_detail'	=> Input::get('web_detail')
                 	 
                 	));
			return Redirect::to('backend/config/enviroment')->with(
				array(
					'save-success' => 'save'
				       ));
		break;
		case 'password':
		 	$password = Input::get('password');
		 	$newpassword = Hash::make($password);
			  if($password ='' || $password ==null)
			  {

			$update=	DB::table('users')
			->where('id',Auth::user()->id)
			->update(
                 array(
                 	'username'	=> Input::get('username')
                 	 
                 	));

			}else{
			$update = DB::table('users')
			->where('id',Auth::user()->id)
			->update(
                 array(
                 	'username'	=> Input::get('username'),
                 	'password'	=> $newpassword
                 	 
                 	));
			}
			return Redirect::to('backend/config/password')->with(
				array(
					'save-success' => 'save'
				       ));
			 
		break;
		case 'categories':
			$url = Helpers::create_url(Input::get('txt_name'));
			$c = new Categories;
			$c->categories_name = Input::get('txt_name');
			$c->categories_url =$url;
			$c->created_at = date('Y-m-d H:i:s');
			$c->updated_at = date('Y-m-d H:i:s');
			$c->create_by =Auth::user()->id;
			$c->save();
			return Redirect::to('backend/config/categories')->with(
				array(
					'save-success' => 'save'
				       ));
		break;
		case 'tag':
			$url = Helpers::create_url(Input::get('txt_name'));
			$c = new Tag;
			$c->tag_name = Input::get('txt_name');
			$c->tag_url =$url;
			$c->created_at = date('Y-m-d H:i:s');
			$c->updated_at = date('Y-m-d H:i:s');
			$c->create_by =Auth::user()->id;
			$c->save();
			return Redirect::to('backend/config/tag')->with(
				array(
					'save-success' => 'save'
				       ));
		break;
	endswitch;	
}
public function getDel($type,$id)
{
	switch($type):
		case 'enviroment':
		break;
		case 'categories':
		Categories::find($id)->delete();
		return Redirect::to('backend/config/categories')->with(
				array(
					'del-success' => 'del'
				       ));
		break;
		case 'tag':
		Tag::find($id)->delete();
		return Redirect::to('backend/config/tag')->with(
				array(
					'del-success' => 'del'
				       ));
		break;
		case 'mainmenu':
		Mainmenu::find($id)->delete();
		Submenu::where('submenu_categories',$id)->delete();
		return Redirect::to('backend/menu/mainmenu')->with(
				array(
					'del-success' => 'del'
				       ));
		break;
		case 'submenu':
		 
		Submenu::find($id)->delete();
		return Redirect::to('backend/menu/submenu')->with(
				array(
					'del-success' => 'del'
				       ));
		break;
		case 'content':
		 
			$c = Content::find($id);
			$token = $c->content_file;
			$img = Uploadfiles::where('token','=',$token)->first();
			$filename='uploadfiles/'.$img->files_newname; 
			File::delete($filename);
			$img = Uploadfiles::where('token',$token)->delete();
			$tag = Tagcontent::where('content_id',$id)->delete();
			$delcontent = Content::find($id)->delete();

		return Redirect::to('backend/content')->with(
				array(
					'del-success' => 'del'
				       ));
		break;
	endswitch;
}
public function postEdit($type)
{
	switch($type):
		case 'enviroment':
		break;
		case 'categories':
		$url = Helpers::create_url(Input::get('txt_name'));
			$id = Input::get('id');
			$c = Categories::find($id);
		
			$c->categories_name = Input::get('txt_name');
			$c->categories_url = $url;
			$c->updated_at = date('Y-m-d H:i:s');
			$c->create_by =Auth::user()->id;
			$c->save();
			return Redirect::to('backend/config/categories')->with(
				array(
					'edit-success' => 'edit'
				       ));
		break;
		case 'tag':
			$id = Input::get('id');
			$c = Tag::find($id);
			$url = Helpers::create_url(Input::get('txt_name'));
		
			$c->tag_name = Input::get('txt_name');
			$c->tag_url = $url;
			$c->updated_at = date('Y-m-d H:i:s');
			$c->create_by =Auth::user()->id;
			$c->save();
			return Redirect::to('backend/config/tag')->with(
				array(
					'edit-success' => 'edit'
				       ));
		break;
	endswitch;
}
 



function getLogout(){
    Auth::logout();
    return Redirect::to('login');
  }

		 
}