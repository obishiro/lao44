<?php

class Submenu extends \Eloquent {
	protected $table = 'tb_submenu';
	public $timestamps = true;

	public function categories_name ()
    	{
        	return $this->hasOne('Mainmenu');
    	}
}