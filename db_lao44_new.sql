-- phpMyAdmin SQL Dump
-- version 4.4.12
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 13, 2015 at 06:39 AM
-- Server version: 5.6.25
-- PHP Version: 5.6.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_lao44`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_categories`
--

CREATE TABLE IF NOT EXISTS `tb_categories` (
  `id` int(25) NOT NULL,
  `categories_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `categories_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `create_by` int(13) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_categories`
--

INSERT INTO `tb_categories` (`id`, `categories_name`, `categories_url`, `created_at`, `updated_at`, `create_by`) VALUES
(4, 'test Edit Categories', 'test-Edit-Categories', '2015-10-25 20:52:46', '2015-11-09 21:15:43', 1),
(5, 'Test save categories', 'Test-save-categories', '2015-10-26 15:00:22', '2015-11-09 21:15:38', 1),
(17, 'Add Again', 'Add-Again', '2015-10-26 15:45:40', '2015-11-09 21:15:33', 1),
(18, 'test', 'test', '2015-10-26 15:46:32', '2015-11-09 21:15:28', 1),
(21, 'test Edit Categories', 'test-Edit-Categories', '2015-10-26 15:57:31', '2015-11-09 21:15:22', 1),
(22, 'Add Again', 'Add-Again', '2015-10-26 15:58:33', '2015-11-09 21:15:18', 1),
(23, 'New Categories', 'New-Categories', '2015-10-26 15:58:40', '2015-11-09 21:15:15', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_content`
--

CREATE TABLE IF NOT EXISTS `tb_content` (
  `id` int(11) NOT NULL,
  `content_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content_categories` int(6) NOT NULL,
  `content_detail` longtext COLLATE utf8_unicode_ci NOT NULL,
  `content_file` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `content_view` int(15) NOT NULL,
  `content_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_by` int(15) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_content`
--

INSERT INTO `tb_content` (`id`, `content_name`, `content_categories`, `content_detail`, `content_file`, `content_view`, `content_url`, `created_at`, `updated_at`, `created_by`) VALUES
(1, 'test Edit Categories', 4, 'Test', 'FRtTpVcHdKhhXiq4', 8, 'test-Edit-Categories', '2015-11-05 23:29:13', '2015-11-12 23:03:07', 1),
(2, 'Test empty file', 4, 'Test', 'kz99wFXk0tY2r0YD', 4, 'Test-empty-file', '2015-11-05 23:30:18', '2015-11-11 15:35:25', 1),
(3, 'New Content to test', 4, '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n', 'D6IYFRb91Az8pOHF', 10, 'New-Content-to-test', '2015-11-11 15:42:05', '2015-11-12 23:28:15', 1),
(4, 'Test Add content agin', 4, '<h1>Getting the Size of a File</h1>\r\n\r\n<p>Problem</p>\r\n\r\n<p>You want to know what the size of a file is.</p>\r\n\r\n<p>Solution</p>\r\n\r\n<p>Use the <code>File::size()</code> method.</p>\r\n\r\n<pre>\r\n$bytes = File::size($filename);</pre>\r\n\r\n<p>Discussion</p>\r\n\r\n<p>Watch out for errors.</p>\r\n\r\n<p>If you try to get the type of non-existent files or files you don&#39;t have access to, errors will occur.</p>\r\n', 'SkuWkikGFVIMgRL7', 5, 'Test-Add-content-agin', '2015-11-11 15:48:19', '2015-11-12 11:13:05', 1),
(5, 'Content Vdo', 4, '', 'QOdipDiZzVvBcven', 4, 'Content-Vdo', '2015-11-12 21:01:37', '2015-11-13 11:07:58', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_enviroment`
--

CREATE TABLE IF NOT EXISTS `tb_enviroment` (
  `id` int(11) NOT NULL,
  `web_name_lo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `web_name_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `web_address` text COLLATE utf8_unicode_ci NOT NULL,
  `web_tel` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `web_email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `web_keyword` text COLLATE utf8_unicode_ci NOT NULL,
  `web_detail` text COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(15) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_enviroment`
--

INSERT INTO `tb_enviroment` (`id`, `web_name_lo`, `web_name_en`, `web_address`, `web_tel`, `web_email`, `web_keyword`, `web_detail`, `created_by`) VALUES
(1, 'ອີກບໍ່ດົນ ເຮົາກໍ່ຈະໄດ້ພົບອິສະຫລະພາບທາງຈິນຕະນາການ', 'test', 'test', '02000000333333333', 'test@gmail.com', 'lao44,uploads', 'lao44', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_files`
--

CREATE TABLE IF NOT EXISTS `tb_files` (
  `id` int(15) NOT NULL,
  `files_newname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `files_oldname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `files_type` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `files_size` int(100) NOT NULL,
  `token` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_files`
--

INSERT INTO `tb_files` (`id`, `files_newname`, `files_oldname`, `files_type`, `files_size`, `token`) VALUES
(1, 'sIEYQdjj5B6pumFl.xlsx', 'การเซ_ตแผน - ความหมาย Role และ Duty.xlsx', 'xlsx', 455440, 'FRtTpVcHdKhhXiq4'),
(2, 'NmQbrx54dWemetvb.xlsx', 'รายงานลงบ_ญช_ฯของ อ.เขวาส_นร_นทร_.xlsx', 'xlsx', 285440, 'kz99wFXk0tY2r0YD'),
(3, '8fq2umc21e8M4DGI.pdf', 'IMG_0001.pdf', 'pdf', 428440, 'D6IYFRb91Az8pOHF'),
(4, '4SCvBz5jmghhFk3u.docx', 'googbooon.docx', 'docx', 845440, 'SkuWkikGFVIMgRL7'),
(5, 'fgtmbtGZeVo8jjx3.MP4', 'ออกพรรษาละ.MP4', 'MP4', 8205502, 'QOdipDiZzVvBcven');

-- --------------------------------------------------------

--
-- Table structure for table `tb_mainmenu`
--

CREATE TABLE IF NOT EXISTS `tb_mainmenu` (
  `id` int(25) NOT NULL,
  `mainmenu_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mainmenu_type` int(6) NOT NULL,
  `mainmenu_detail` longtext COLLATE utf8_unicode_ci,
  `mainmenu_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `m_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mainmenu_sorting` int(6) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `create_by` int(13) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_mainmenu`
--

INSERT INTO `tb_mainmenu` (`id`, `mainmenu_name`, `mainmenu_type`, `mainmenu_detail`, `mainmenu_url`, `m_url`, `mainmenu_sorting`, `created_at`, `updated_at`, `create_by`) VALUES
(4, 'Main Menu', 3, '', 'http://', '', 0, '2015-10-31 19:25:51', '2015-10-31 19:42:53', 1),
(5, 'Main menu', 3, '', 'http://', 'Main-menu', 0, '2015-11-06 14:35:54', '2015-11-06 14:35:54', 1),
(6, 'MainmenuContent', 1, '', 'http://', 'MainmenuContent', 0, '2015-11-09 20:36:29', '2015-11-09 20:36:29', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_submenu`
--

CREATE TABLE IF NOT EXISTS `tb_submenu` (
  `id` int(25) NOT NULL,
  `submenu_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `submenu_categories` int(6) NOT NULL,
  `submenu_type` int(6) NOT NULL,
  `submenu_detail` longtext COLLATE utf8_unicode_ci,
  `submenu_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `s_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `create_by` int(13) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_submenu`
--

INSERT INTO `tb_submenu` (`id`, `submenu_name`, `submenu_categories`, `submenu_type`, `submenu_detail`, `submenu_url`, `s_url`, `created_at`, `updated_at`, `create_by`) VALUES
(1, 'Test Sub Link', 4, 2, '', 'http://www.google.com', 'Test-Sub-Link', '2015-10-31 19:01:16', '2015-11-12 21:45:25', 1),
(4, 'test Sub menu', 4, 1, '<p>Views typically contain the HTML of your application and provide a convenient way of separating your controller and domain logic from your presentation logic. Views are stored in the <code>app/views</code> directory.</p>\r\n\r\n<p>A simple view could look something like this:</p>\r\n\r\n<pre>\r\n<code>&lt;!-- View stored in app/views/greeting.php --&gt;\r\n\r\n&lt;html&gt;\r\n    &lt;body&gt;\r\n        &lt;h1&gt;Hello, &lt;?php echo $name; ?&gt;&lt;/h1&gt;\r\n    &lt;/body&gt;\r\n&lt;/html&gt;</code></pre>\r\n', 'http://www.google.com', 'test-Sub-menu', '2015-10-31 19:43:08', '2015-11-12 21:45:04', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_tag`
--

CREATE TABLE IF NOT EXISTS `tb_tag` (
  `id` int(25) NOT NULL,
  `tag_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tag_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tag_count` int(15) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `create_by` int(13) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_tag`
--

INSERT INTO `tb_tag` (`id`, `tag_name`, `tag_url`, `tag_count`, `created_at`, `updated_at`, `create_by`) VALUES
(1, 'PHP', 'PHP', 9, '2015-11-04 14:12:06', '2015-11-12 23:24:21', 1),
(2, 'Laravel55', 'Laravel55', 3, '2015-11-04 14:12:15', '2015-11-12 23:23:57', 1),
(3, 'Tag Name', 'Tag-Name', 1, '2015-11-12 23:02:58', '2015-11-12 23:23:59', 1),
(4, 'Tag Again', 'Tag-Again', 2, '2015-11-12 23:20:19', '2015-11-12 23:24:07', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_tagcontent`
--

CREATE TABLE IF NOT EXISTS `tb_tagcontent` (
  `id` int(11) NOT NULL,
  `tag_id` int(6) NOT NULL,
  `content_id` int(15) NOT NULL,
  `tag_count` int(25) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_tagcontent`
--

INSERT INTO `tb_tagcontent` (`id`, `tag_id`, `content_id`, `tag_count`) VALUES
(1, 2, 1, 0),
(2, 1, 1, 0),
(3, 2, 3, 0),
(4, 1, 3, 0),
(5, 2, 4, 0),
(6, 1, 5, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(25) NOT NULL,
  `username` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_status` int(3) NOT NULL,
  `user_type` int(11) NOT NULL,
  `user_code` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `user_status`, `user_type`, `user_code`, `created_at`, `updated_at`, `remember_token`) VALUES
(1, 'admin', '$2y$10$tO8QkExsueZVRDDWKuDHmeIt.dE6s6jI0lgvFADE4ANU8ZNDpYKlu', 1, 1, '', '2015-10-20 00:00:00', '2015-11-06 14:42:43', 'UHWiXXIFWAzam7duHCKgARjaZVjS27DZLCsmRq5qaeS4Ip02AgI6U7kBvGF6');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_categories`
--
ALTER TABLE `tb_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_content`
--
ALTER TABLE `tb_content`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_enviroment`
--
ALTER TABLE `tb_enviroment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_files`
--
ALTER TABLE `tb_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_mainmenu`
--
ALTER TABLE `tb_mainmenu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_submenu`
--
ALTER TABLE `tb_submenu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_tag`
--
ALTER TABLE `tb_tag`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_tagcontent`
--
ALTER TABLE `tb_tagcontent`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_categories`
--
ALTER TABLE `tb_categories`
  MODIFY `id` int(25) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `tb_content`
--
ALTER TABLE `tb_content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tb_enviroment`
--
ALTER TABLE `tb_enviroment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_files`
--
ALTER TABLE `tb_files`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tb_mainmenu`
--
ALTER TABLE `tb_mainmenu`
  MODIFY `id` int(25) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tb_submenu`
--
ALTER TABLE `tb_submenu`
  MODIFY `id` int(25) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_tag`
--
ALTER TABLE `tb_tag`
  MODIFY `id` int(25) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_tagcontent`
--
ALTER TABLE `tb_tagcontent`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(25) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
