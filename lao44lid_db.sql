-- phpMyAdmin SQL Dump
-- version 4.5.0.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 13, 2016 at 07:43 PM
-- Server version: 5.5.31-log
-- PHP Version: 5.5.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lao44lid_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_categories`
--

CREATE TABLE `tb_categories` (
  `id` int(25) NOT NULL,
  `categories_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `categories_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `categories_show` int(2) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `create_by` int(13) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_categories`
--

INSERT INTO `tb_categories` (`id`, `categories_name`, `categories_url`, `categories_show`, `created_at`, `updated_at`, `create_by`) VALUES
(24, 'ປູກຝັງ (Cropping) ', 'ປູກຝັງ-(Cropping)-', 0, '2015-12-21 10:15:31', '2015-12-21 10:15:31', 1),
(25, 'ລ້ຽງສັດ ແລະ ການປະມົງ (Livestock and Fishery)', 'ລ້ຽງສັດ-ແລະ-ການປະມົງ-(Livestock-and-Fishery)', 0, '2015-12-21 10:33:48', '2015-12-21 10:33:48', 1),
(26, 'ປ່າໄມ້ (Forestry)', 'ປ່າໄມ້-(Forestry)', 0, '2015-12-21 10:36:45', '2015-12-21 10:36:45', 1),
(27, 'ບັນຫາກ່ຽວກັບດິນ (Land issues)', 'ບັນຫາກ່ຽວກັບດິນ-(Land-issues)', 0, '2015-12-21 10:38:44', '2015-12-22 14:25:16', 1),
(28, 'ນິຕິກຳຕ່າງໆ (Legislation)', 'ນິຕິກຳຕ່າງໆ-(Legislation)', 0, '2015-12-21 10:50:51', '2015-12-22 13:49:27', 1),
(29, 'ສື່ສັງຄົມອອນລ້າຍ (Social Media)', 'ສື່ສັງຄົມອອນລ້າຍ-(Social-Media)', 0, '2015-12-21 10:53:15', '2015-12-21 10:53:15', 1),
(30, 'ການຮັກສາສຸຂະພາບ (Healthcare)', 'ການຮັກສາສຸຂະພາບ-(Healthcare)', 0, '2015-12-21 16:47:14', '2015-12-21 16:47:14', 1),
(31, 'ພູມປັນຍາທ້ອງຖີ່ນ (Local and indeginous knowledge)', 'ພູມປັນຍາທ້ອງຖີ່ນ-(Local-and-indeginous-knowledge)', 0, '2015-12-21 16:56:43', '2015-12-22 13:58:16', 1),
(32, 'ບົດບາດຍີງຊາຍ (Gender)', 'ບົດບາດຍີງຊາຍ-(Gender)', 0, '2015-12-21 16:57:10', '2015-12-21 16:57:10', 1),
(33, 'ສື່ການຮຽນການສອນ (Learning and Teaching materials)', 'ສື່ການຮຽນການສອນ-(Learning-and-Teaching-materials)', 0, '2015-12-21 18:04:17', '2015-12-23 11:35:50', 1),
(34, 'ບັນຫານໍ້າ (water issues)', 'ບັນຫານໍ້າ-(water-issues)', 0, '2015-12-22 14:08:13', '2015-12-22 14:24:34', 1),
(35, 'ບັນຫາສັງຄົມ (Social issues)', 'ບັນຫາສັງຄົມ-(Social-issues)', 0, '2015-12-22 14:24:03', '2015-12-22 14:24:03', 1),
(36, 'ສີນລະປະວັດທະນາທຳລາວ (Lao Art and Culture)', 'ສີນລະປະວັດທະນາທຳລາວ-(Lao-Art-and-Culture)', 0, '2015-12-23 11:23:45', '2015-12-23 11:23:45', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_content`
--

CREATE TABLE `tb_content` (
  `id` int(11) NOT NULL,
  `content_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content_categories` int(6) NOT NULL,
  `content_detail` longtext COLLATE utf8_unicode_ci NOT NULL,
  `content_file` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `content_view` int(15) NOT NULL,
  `content_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_by` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_content`
--

INSERT INTO `tb_content` (`id`, `content_name`, `content_categories`, `content_detail`, `content_file`, `content_view`, `content_url`, `created_at`, `updated_at`, `created_by`) VALUES
(6, 'ຫລັກການປະຕິບັດເພື່ອສົ່ງເສີມຄວາມກ້າວຫນ້າໃຫ້ແກ່ເພດຍີງ Code of conduct for women advancement in your office', 0, '', 'OXaFCbulRcChEcQy', 0, 'ຫລັກການປະຕິບັດເພື່ອສົ່ງເສີມຄວາມກ້າວຫນ້າໃຫ້ແກ່ເພດຍີງ-Code-of-conduct-for-women-advancement-in-your-office', '2015-12-21 09:24:07', '2015-12-21 09:24:07', 1),
(7, 'ບໍ່ມີຄວາມລັບໃນເຟດສບຸກສະບັບພາສາລາວ (There is no secrete on Facebook-Lao sub-title)', 29, '', 'NWjSn0IooQ1Pt9n6', 4, 'ບໍ່ມີຄວາມລັບໃນເຟດສບຸກສະບັບພາສາລາວ-(There-is-no-secrete-on-Facebook-Lao-sub-title)', '2015-12-21 11:04:37', '2015-12-21 17:47:26', 1),
(8, 'ຄິດໃຫ້ດີກ່ອນເຊື່ອຂ່າວໃນເຟດສບຸກ ສະບັບພາສາລາວ (Think before you believe content in Facebook-Lao subtitle)', 29, '', 'PmBOIhOdClSzIPdU', 0, 'ຄິດໃຫ້ດີກ່ອນເຊື່ອຂ່າວໃນເຟດສບຸກ-ສະບັບພາສາລາວ-(Think-before-you-believe-content-in-Facebook-Lao-subtitle)', '2015-12-21 11:23:04', '2015-12-21 17:46:50', 1),
(9, 'ກໍລະນີສຶກສາກ່ຽວກັບມູນຄ່າຂອງເຄື່ອງປ່າຂອງດົງ ທີ່ບ້ານນາເມືອງ (A case study of NTFP value in Namueng village)', 26, '', '5jT0TfF1VqUZ67JL', 1, 'ກໍລະນີສຶກສາກ່ຽວກັບມູນຄ່າຂອງເຄື່ອງປ່າຂອງດົງ-ທີ່ບ້ານນາເມືອງ-(A-case-study-of-NTFP-value-in-Namueng-village)', '2015-12-21 11:27:31', '2015-12-21 17:45:54', 1),
(10, 'ຜົນການສຶກສາກ່ຽວກັບຜົນກະທົບຂອງການ ປູກກ້ວຍທີ່ແຂວງອຸດົມໄຊ (A study on impact of banana plantation in Oudomxay province)', 27, '', '12f12gibDNJRvMm8', 0, 'ຜົນການສຶກສາກ່ຽວກັບຜົນກະທົບຂອງການ-ປູກກ້ວຍທີ່ແຂວງອຸດົມໄຊ-(A-study-on-impact-of-banana-plantation-in-Oudomxay-province)', '2015-12-21 11:29:33', '2015-12-21 17:45:01', 1),
(11, 'ຄິດໃຫ້ດີກ່ອນແຊຂໍ້ມູນໃນເຟດສບຸກ ສະບັບພາສາລາວ (Think before you share content in Facebook-Lao subtitle)', 29, '', 'F8Gs9AFGX3ZWQXOU', 0, 'ຄິດໃຫ້ດີກ່ອນແຊຂໍ້ມູນໃນເຟດສບຸກ-ສະບັບພາສາລາວ-(Think-before-you-share-content-in-Facebook-Lao-subtitle)', '2015-12-21 16:44:31', '2015-12-21 17:43:40', 1),
(12, 'ດີນຂອງຟ້າ (The land of the sky)', 27, '', 'MUbqFh6kK4mYeC7v', 1, 'ດີນຂອງຟ້າ-(The-land-of-the-sky)', '2015-12-21 17:00:01', '2015-12-21 20:43:09', 1),
(13, 'ຂະບວນການສ້າງແຜນທີ່ຂັ້ນບ້ານແບບມີສ່ວນຮ່ວມ (participatory village mapping process)', 27, '', 'pJbF0KOGrCAwfDCT', 0, 'ຂະບວນການສ້າງແຜນທີ່ຂັ້ນບ້ານແບບມີສ່ວນຮ່ວມ-(participatory-village-mapping-process)', '2015-12-21 17:52:45', '2015-12-21 17:52:45', 1),
(14, 'ການປູກຖົ່ວງອກ (bean sprout production) ', 31, '', 'gfPLcA3rnBYOHcoT', 0, 'ການປູກຖົ່ວງອກ-(bean-sprout-production)-', '2015-12-21 18:01:35', '2015-12-21 18:01:35', 1),
(15, 'ຮຽນຄຳສັບບົດທີ3: ຄຳສັບກ່ຽວກັບສັດ (animal vocabulary)', 33, '', 'wfzDbR0Lx1XZFRsc', 0, 'ຮຽນຄຳສັບບົດທີ3:-ຄຳສັບກ່ຽວກັບສັດ-(animal-vocabulary)', '2015-12-21 18:07:31', '2015-12-22 12:29:44', 1),
(16, 'ຮຽນຄຳສັບບົດທີ1: ຄຳສັບສີ່ງຂອງໃນເຮືອນ (vocabulary about things in home)', 33, '', 'X6VNuZlc8ZZ3Rtgm', 0, 'ຮຽນຄຳສັບບົດທີ1:-ຄຳສັບສີ່ງຂອງໃນເຮືອນ-(vocabulary-about-things-in-home)', '2015-12-21 18:09:05', '2015-12-22 12:25:20', 1),
(17, 'ຮຽນຄຳສັບບົດທີ 2: ຄຳສັບກ່ຽວກັບສີ່ງຂອງໃນເຮືອນຄົວ ( Vocabulary about things in kitchen)', 33, '', 'DQpwtEw1iOJ4AGZl', 0, 'ຮຽນຄຳສັບບົດທີ-2:-ຄຳສັບກ່ຽວກັບສີ່ງຂອງໃນເຮືອນຄົວ-(-Vocabulary-about-things-in-kitchen)', '2015-12-21 18:10:22', '2015-12-22 12:24:57', 1),
(18, 'ຮຽນຄຳສັບບົດທີ 4: ຄຳສັບກ່ຽວກັບສັດນໍ້າ (aquatic vocabulary)', 33, '', 'IOrVdaL6IhkDcTQt', 0, 'ຮຽນຄຳສັບບົດທີ-4:-ຄຳສັບກ່ຽວກັບສັດນໍ້າ-(aquatic-vocabulary)', '2015-12-21 18:11:31', '2015-12-22 12:24:35', 1),
(19, 'ຮຽນຄຳສັບບົດທີ 5: ຄຳສັບກ່ຽວກັບອາຫານ (Vocabulary about food)', 33, '', '0Okr2bYYyE4Y81wJ', 2, 'ຮຽນຄຳສັບບົດທີ-5:-ຄຳສັບກ່ຽວກັບອາຫານ-(Vocabulary-about-food)', '2015-12-21 18:12:43', '2015-12-22 12:24:14', 1),
(20, 'ຮຽນຄຳສັບບົດທີ 6: ຄຳສັບກ່ຽວກັບເຄື່ອງດື່ມ (Vocabulary about beverage)', 33, '', 'kOqErO4Ro3GMZ6yC', 0, 'ຮຽນຄຳສັບບົດທີ-6:-ຄຳສັບກ່ຽວກັບເຄື່ອງດື່ມ-(Vocabulary-about-beverage)', '2015-12-21 18:14:23', '2015-12-22 12:24:04', 1),
(21, 'ຮຽນຄຳສັບບົດທີ 7: ຄຳສັບກ່ຽວກັບຜັກ (Vocabulary about vegetable)', 33, '', '0azn3SjpZySRfGMP', 2, 'ຮຽນຄຳສັບບົດທີ-7:-ຄຳສັບກ່ຽວກັບຜັກ-(Vocabulary-about-vegetable)', '2015-12-21 18:15:17', '2015-12-22 12:21:17', 1),
(22, 'ຮຽນຄຳສັບບົດທີ 8: ຄຳສັບກ່ຽວກັບຫມາກໄມ້່ (Vocabulary about fruits)', 33, '', 'CGJdRReNUkUF9zjt', 0, 'ຮຽນຄຳສັບບົດທີ-8:-ຄຳສັບກ່ຽວກັບຫມາກໄມ້່-(Vocabulary-about-fruits)', '2015-12-22 12:34:48', '2015-12-22 12:34:48', 1),
(23, 'ຮຽນຄຳສັບບົດທີ 9: ຄຳສັບກ່ຽວກັບຄອບຄົວ (Vocabulary about family)', 33, '', 'bDsHGUidG2YY8e5i', 0, 'ຮຽນຄຳສັບບົດທີ-9:-ຄຳສັບກ່ຽວກັບຄອບຄົວ-(Vocabulary-about-family)', '2015-12-22 12:42:51', '2015-12-22 12:42:51', 1),
(24, 'ຮຽນຄຳສັບບົດທີ 10: ຄຳສັບກ່ຽວກັບການຕັ້ງຄຳຖາມ (Vocabulary about questions)', 33, '', 'ncs2VUo4GmaIksut', 0, 'ຮຽນຄຳສັບບົດທີ-10:-ຄຳສັບກ່ຽວກັບການຕັ້ງຄຳຖາມ-(Vocabulary-about-questions)', '2015-12-22 13:16:25', '2015-12-22 13:16:25', 1),
(26, 'ຮຽນຄຳສັບບົດທີ 11: ຄຳສັບກ່ຽວກັບສະຖານທີ່ (Vocabulary about locations)', 33, '', 'CyIOAfQ8hbxjmbiE', 0, 'ຮຽນຄຳສັບບົດທີ-11:-ຄຳສັບກ່ຽວກັບສະຖານທີ່-(Vocabulary-about-locations)', '2015-12-22 13:20:37', '2015-12-22 13:20:37', 1),
(27, 'ຮຽນຄຳສັບບົດທີ 12: ຄຳສັບກ່ຽວກັບການເຄື່ອນໄຫວ (Vocabulary about actions)', 33, '', 'USJ5EK6MjJS3wd6X', 1, 'ຮຽນຄຳສັບບົດທີ-12:-ຄຳສັບກ່ຽວກັບການເຄື່ອນໄຫວ-(Vocabulary-about-actions)', '2015-12-22 13:26:44', '2015-12-22 13:28:58', 1),
(28, 'ຮຽນຄຳສັບບົດທີ 14: ຄຳສັບກ່ຽວກັບສີ (Vocabulary about colors)', 33, '', 'iTM9uJMAqsIb2sR7', 0, 'ຮຽນຄຳສັບບົດທີ-14:-ຄຳສັບກ່ຽວກັບສີ-(Vocabulary-about-colors)', '2015-12-22 13:34:52', '2015-12-22 13:34:52', 1),
(29, 'ຮຽນຄຳສັບບົດທີ 13: ຄຳສັບກ່ຽວກັບຄວາມຮູ້ສຶກ (Vocabulary about emotions)', 33, '', 'LkY1Nhek3IDIPLMu', 0, 'ຮຽນຄຳສັບບົດທີ-13:-ຄຳສັບກ່ຽວກັບຄວາມຮູ້ສຶກ-(Vocabulary-about-emotions)', '2015-12-22 13:35:02', '2015-12-22 13:35:02', 1),
(30, 'ຮຽນຄຳສັບບົດທີ 15: ຄຳສັບກ່ຽວກັບເວລາ (Vocabulary about time)', 33, '', '7N74qmnMKQTW8axy', 0, 'ຮຽນຄຳສັບບົດທີ-15:-ຄຳສັບກ່ຽວກັບເວລາ-(Vocabulary-about-time)', '2015-12-22 13:38:35', '2015-12-22 13:38:35', 1),
(31, 'ຮຽນຄຳສັບບົດທີ 16: ຄຳສັບກ່ຽວກັບໂຮງຮຽນ (Vocabulary about school)', 33, '', 'hjjnLMQAJqDA2bL9', 0, 'ຮຽນຄຳສັບບົດທີ-16:-ຄຳສັບກ່ຽວກັບໂຮງຮຽນ-(Vocabulary-about-school)', '2015-12-22 13:40:04', '2015-12-22 13:40:04', 1),
(32, 'ຮຽນຄຳສັບບົດທີ 19: ຄຳສັບກ່ຽວກັບງານບຸນຕ່າງໆ (Vocabulary about festivals)', 33, '', 'l9cGxiUYKXu5dfuP', 0, 'ຮຽນຄຳສັບບົດທີ-19:-ຄຳສັບກ່ຽວກັບງານບຸນຕ່າງໆ-(Vocabulary-about-festivals)', '2015-12-22 13:43:36', '2015-12-22 13:43:36', 1),
(33, 'ຮຽນຄຳສັບບົດທີ 18: ຄຳສັບກ່ຽວກັບວັດ (Vocabulary about temple)', 33, '', '9zVt4tC6bt1U7S83', 0, 'ຮຽນຄຳສັບບົດທີ-18:-ຄຳສັບກ່ຽວກັບວັດ-(Vocabulary-about-temple)', '2015-12-22 13:43:45', '2015-12-22 13:43:45', 1),
(34, 'ຮຽນຄຳສັບບົດທີ 20: ຄຳສັບກ່ຽວກັບການຊ່ວຍເຫລືອເດັກນ້ອຍຫູຫນວກ (Vocabulary about helping deaf children)', 33, '', 'rAtHw0C1v5fI8kqi', 1, 'ຮຽນຄຳສັບບົດທີ-20:-ຄຳສັບກ່ຽວກັບການຊ່ວຍເຫລືອເດັກນ້ອຍຫູຫນວກ-(Vocabulary-about-helping-deaf-children)', '2015-12-22 13:47:14', '2015-12-23 14:15:07', 1),
(35, 'ສິດຂອງຊາວບ້ານທີ່ຖືກຜົນກະທົບຈາາກໂຄງການພັດທະນາ (Rights to compensation from development projects)', 28, '', '6K4OC14GN5DQ8CSw', 0, 'ສິດຂອງຊາວບ້ານທີ່ຖືກຜົນກະທົບຈາາກໂຄງການພັດທະນາ-(Rights-to-compensation-from-development-projects)', '2015-12-22 13:54:50', '2015-12-22 13:55:20', 1),
(36, 'ນິທານພື້ນບ້ານເຜົ່າຕາໂອຍ (Folk story of Taouy people)', 31, '', '6RI2rVPxIJrGRiLR', 0, 'ນິທານພື້ນບ້ານເຜົ່າຕາໂອຍ-(Folk-story-of-Taouy-people)', '2015-12-22 14:00:54', '2015-12-22 14:00:54', 1),
(37, 'ແຜ່ນດິນຄືຊີວິດ (Land is life)', 27, '', 'dS1OZCc8vI3VHoHo', 2, 'ແຜ່ນດິນຄືຊີວິດ-(Land-is-life)', '2015-12-22 14:04:20', '2016-01-13 16:39:37', 1),
(38, 'ຢູ່ໄກແພດ (Where s no doctor)', 30, '', 'wjiWWumCtVyT8Nq7', 1, 'ຢູ່ໄກແພດ-(Where-s-no-doctor)', '2015-12-22 14:06:12', '2015-12-22 14:10:07', 1),
(39, 'ລິຂະສິດ (Intellectual and property rights)', 28, '', 'DoKKErz8VPOO7G5E', 0, 'ລິຂະສິດ-(Intellectual-and-property-rights)', '2015-12-22 14:15:26', '2015-12-22 14:15:26', 1),
(40, 'ໂພສະນາການ Nutrition', 33, '', '0C6pOzvWolGamzks', 0, 'ໂພສະນາການ-Nutrition', '2015-12-22 14:17:52', '2015-12-22 14:18:18', 1),
(41, 'ຄິດກ່ອນເຮັດ (Thinks before you act)', 34, '', 'zGhffpoHSy4JLgGH', 0, 'ຄິດກ່ອນເຮັດ-(Thinks-before-you-act)', '2015-12-22 15:03:40', '2015-12-22 15:04:31', 1),
(42, 'ໂລກເບົາຫວານຕອນທີ 2 (Diabetes education part 2)', 30, '', 'a5KstiVAEqCcWWMx', 0, 'ໂລກເບົາຫວານຕອນທີ-2-(Diabetes-education-part-2)', '2015-12-22 15:05:10', '2015-12-22 15:05:10', 1),
(43, 'ໂລກເບົາຫວານຕອນທີ 1 (Diabetes education part 1)', 30, '', 'XWkuOL2KX6wzaUbi', 0, 'ໂລກເບົາຫວານຕອນທີ-1-(Diabetes-education-part-1)', '2015-12-22 15:33:59', '2015-12-22 15:33:59', 1),
(44, 'ໂລກເບົາຫວານຕອນທີ 3 (Diabetes education part 3)', 30, '', 'bLOAZ7kkwkSAMOr8', 0, 'ໂລກເບົາຫວານຕອນທີ-3-(Diabetes-education-part-3)', '2015-12-22 15:55:09', '2015-12-22 15:55:09', 1),
(45, 'ໂລກເບົາຫວານຕອນທີ 4 (Diabetes education part 4)', 30, '', 'a1vnUEqzAccB1CkD', 0, 'ໂລກເບົາຫວານຕອນທີ-4-(Diabetes-education-part-4)', '2015-12-22 15:55:18', '2015-12-22 15:55:18', 1),
(46, 'ບຸນມີມິຄວາຍໂຕຫນື່ງ (Bounmy has a buffalo)', 33, '', 'uuKI69X8gWClWZmQ', 2, 'ບຸນມີມິຄວາຍໂຕຫນື່ງ-(Bounmy-has-a-buffalo)', '2015-12-22 15:59:56', '2016-01-08 17:02:22', 1),
(47, 'ບຸນມີມີຄວາມຫນື່ງໂຕ (Bounmy has a buffalo)', 33, '', 'n3iZJUjUcUSBu4o5', 0, 'ບຸນມີມີຄວາມຫນື່ງໂຕ-(Bounmy-has-a-buffalo)', '2015-12-22 16:03:06', '2015-12-22 16:03:06', 1),
(48, 'ການຫາຍຕົວຂອງສີ (Color is disappeared) ', 33, '', 'mlym5EP6zJutflR6', 0, 'ການຫາຍຕົວຂອງສີ-(Color-is-disappeared)-', '2015-12-22 16:50:40', '2015-12-22 16:50:40', 1),
(49, 'ພາສາອັງກິດສຳລັບຜູ້ນຳທ່ຽວແບບອານຸລັກ (English for Eco-Guides)', 33, '', 'zedoPDwaoqH80lM8', 0, 'ພາສາອັງກິດສຳລັບຜູ້ນຳທ່ຽວແບບອານຸລັກ-(English-for-Eco-Guides)', '2015-12-22 16:52:50', '2015-12-22 16:52:50', 1),
(50, 'ການລະເຫີຍຂອງອາຍ (Heat disappearance)', 33, '', 'w08XoulUWEJ1bUio', 0, 'ການລະເຫີຍຂອງອາຍ-(Heat-disappearance)', '2015-12-23 11:19:08', '2015-12-23 11:19:32', 1),
(51, 'ເລກມາຍາກົນພາກທີ 1 (Magic numbers 1)', 33, '', '2tB3gfqa3DOwWh6U', 0, 'ເລກມາຍາກົນພາກທີ-1-(Magic-numbers-1)', '2015-12-23 11:20:42', '2015-12-23 11:21:59', 1),
(52, 'ເລກມາຍາກົນພາກ 2 (Magic numbers part 2)', 33, '', 'HK5gNPK7u8lBtQJW', 0, 'ເລກມາຍາກົນພາກ-2-(Magic-numbers-part-2)', '2015-12-23 11:21:39', '2015-12-23 11:21:39', 1),
(53, 'ບົດຟ້ອນຈຳປາເມືອງລາວ (Champa dance)', 36, '', 'T7WLbGVNj6vWap61', 0, 'ບົດຟ້ອນຈຳປາເມືອງລາວ-(Champa-dance)', '2015-12-23 11:25:55', '2015-12-23 11:25:55', 1),
(54, 'ຮຽນພະຍັນຊະນະລາວໃນ2ນາທີ (Learning Lao in 2 minutes)', 33, '', 'VSNWbO51d0w8rha9', 0, 'ຮຽນພະຍັນຊະນະລາວໃນ2ນາທີ-(Learning-Lao-in-2-minutes)', '2015-12-23 11:27:13', '2015-12-23 11:27:13', 1),
(55, 'ປື້ມຫລີ້ນເກມກ່ຽວກັບລີງ (Silvered langur activity book)', 33, '', '8OsmexR9LHTkpRyj', 0, 'ປື້ມຫລີ້ນເກມກ່ຽວກັບລີງ-(Silvered-langur-activity-book)', '2015-12-23 11:30:43', '2015-12-23 11:30:43', 1),
(56, 'ຊ່ວຍຄຸ້ມຄອງດຸກກີ້ ລີງຂາແດງນ້ອຍຜູ້ຫນ້າສົງສານ (Save Doucki)', 33, '', 'ZlLIASg969n4zbwv', 2, 'ຊ່ວຍຄຸ້ມຄອງດຸກກີ້-ລີງຂາແດງນ້ອຍຜູ້ຫນ້າສົງສານ-(Save-Doucki)', '2015-12-23 11:35:11', '2015-12-23 14:28:20', 1),
(57, 'ພາສາອັງກິດກ່ຽວກັບວັດທະນາທຳອາເມລິກາ (English on American culture)', 33, '', '6irJkEnhjqntbrNB', 0, 'ພາສາອັງກິດກ່ຽວກັບວັດທະນາທຳອາເມລິກາ-(English-on-American-culture)', '2015-12-23 11:40:41', '2015-12-23 11:40:41', 1),
(58, 'ພາສາອັງກິດກ່ຽວກັບຄຳນຳຫນ້າ (Use of Article)', 33, '', 'QpNxmR3qrPv44B7B', 0, 'ພາສາອັງກິດກ່ຽວກັບຄຳນຳຫນ້າ-(Use-of-Article)', '2015-12-23 11:41:42', '2015-12-23 11:41:42', 1),
(59, 'ພາສາອັງກິດກ່ຽວກັບການຕັ້ງຄຳຖາມ (Ask better questions)', 33, '', 'YNpS7EKr4pzTzZ6A', 0, 'ພາສາອັງກິດກ່ຽວກັບການຕັ້ງຄຳຖາມ-(Ask-better-questions)', '2015-12-23 11:43:07', '2015-12-23 11:43:07', 1),
(60, 'ພາສາອັງກິດກ່ຽວກັບການນຳໃຊ້ Should, Could, Would (Use of Should, Could, Would)', 33, '', 'xGqZaPXGg3MAL37b', 0, 'ພາສາອັງກິດກ່ຽວກັບການນຳໃຊ້-Should,-Could,-Would-(Use-of-Should,-Could,-Would)', '2015-12-23 11:44:36', '2015-12-23 11:44:36', 1),
(61, 'ການນຳໃຊ້ສຳນວນ Hit on them', 33, '', 'sm5BwS9CZ7mDljkV', 1, 'ການນຳໃຊ້ສຳນວນ-Hit-on-them', '2015-12-23 11:46:09', '2015-12-23 11:46:53', 1),
(62, 'ການນຳໃຊ້ສຳນວນHow it going', 33, '', 'l5uSsX0q0Dn92awC', 0, 'ການນຳໃຊ້ສຳນວນHow-it-going', '2015-12-23 11:48:01', '2015-12-23 11:48:01', 1),
(63, 'ພາສາລາວກັບພາສາບາຊີ້ວ Lao VS Brazilian ', 33, '', 'wkv1OYRm0bvHZuDh', 4, 'ພາສາລາວກັບພາສາບາຊີ້ວ-Lao-VS-Brazilian-', '2015-12-23 11:49:50', '2016-01-13 16:42:01', 1),
(64, 'ການອອກສຽງໂຕ F ແລະ ໂຕ V (How to pronounce F and V) ', 33, '', 'PZ1j3SOy2viCEVY0', 28, 'ການອອກສຽງໂຕ-F-ແລະ-ໂຕ-V-(How-to-pronounce-F-and-V)-', '2015-12-23 11:51:22', '2016-01-13 19:21:24', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_enviroment`
--

CREATE TABLE `tb_enviroment` (
  `id` int(11) NOT NULL,
  `web_name_lo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `web_name_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `web_address` text COLLATE utf8_unicode_ci NOT NULL,
  `web_tel` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `web_email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `web_keyword` text COLLATE utf8_unicode_ci NOT NULL,
  `web_detail` text COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_enviroment`
--

INSERT INTO `tb_enviroment` (`id`, `web_name_lo`, `web_name_en`, `web_address`, `web_tel`, `web_email`, `web_keyword`, `web_detail`, `created_by`) VALUES
(1, 'ລາວ44', 'Lao44', 'test', '02000000333333333', 'test@gmail.com', 'lao44,uploads', 'lao44', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_files`
--

CREATE TABLE `tb_files` (
  `id` int(15) NOT NULL,
  `files_newname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `files_oldname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `files_type` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `files_size` int(100) NOT NULL,
  `token` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_files`
--

INSERT INTO `tb_files` (`id`, `files_newname`, `files_oldname`, `files_type`, `files_size`, `token`) VALUES
(6, 'FA9mTLZkXtaGoq70.pdf', 'coc-final.max quality.pdf', 'pdf', 2911265, 'OXaFCbulRcChEcQy'),
(7, 'U8hJJhHbaJbGca7i.mp4', 'No secrete on facebook.lao sub.mp4', 'mp4', 61019827, 'NWjSn0IooQ1Pt9n6'),
(8, '5DTJtKO06h5h4YHz.mp4', 'Think before you believe.lao sub.mp4', 'mp4', 36243843, 'PmBOIhOdClSzIPdU'),
(9, 'pzdAdnNDPbtECX4v.pdf', 'NTFP value JIRCAS (Lao).pdf', 'pdf', 4731221, '5jT0TfF1VqUZ67JL'),
(10, 'XpBB16vUXai9c6jt.pdf', 'LIWG Study Session_Satomi (Lao).pdf', 'pdf', 1413988, '12f12gibDNJRvMm8'),
(11, 'EZ325rjOiyVFl1TT.mp4', 'Think before you share.lao sub.mp4', 'mp4', 87860360, 'F8Gs9AFGX3ZWQXOU'),
(12, 'xCNjfEZTYEj7nFht.pdf', 'creative-writers-group-land-of-the-sky.pdf', 'pdf', 8966593, 'MUbqFh6kK4mYeC7v'),
(13, 'F6fjG6mtO2gEGnbS.mp4', 'dok-lao-community-learning.mp4', 'mp4', 43106817, 'pJbF0KOGrCAwfDCT'),
(14, '', '', '0', 0, 'gfPLcA3rnBYOHcoT'),
(15, '2bYT4QcwqlMoMnj0.mp4', 'find-in-laos-03-land-animals.mp4', 'mp4', 6292088, 'wfzDbR0Lx1XZFRsc'),
(16, 'kuIGL1QPXuFQnMij.mp4', 'find-in-laos-01-things-in-the-home.mp4', 'mp4', 11034659, 'X6VNuZlc8ZZ3Rtgm'),
(17, 'cZKr7mKLh4TpDoBB.mp4', 'find-in-laos-02-vocabulary-about-the-kitchen.mp4', 'mp4', 4307173, 'DQpwtEw1iOJ4AGZl'),
(18, 'bdN4Kz27OQvBJtOD.mp4', 'find-in-laos-04-water-animals.mp4', 'mp4', 5768359, 'IOrVdaL6IhkDcTQt'),
(19, 'qpTOxi0XnaxG0ta3.mp4', 'find-in-laos-05-food.mp4', 'mp4', 11189368, '0Okr2bYYyE4Y81wJ'),
(20, 'bgdI9sC0cOsSQb2e.mp4', 'find-in-laos-06-beverages.mp4', 'mp4', 4663461, 'kOqErO4Ro3GMZ6yC'),
(21, 'ZNZas9WIOjlgC3Vf.mp4', 'find-in-laos-07-vegetables.mp4', 'mp4', 9802862, '0azn3SjpZySRfGMP'),
(22, 'EJQ5c7l4HXPbCH27.mp4', 'find-in-laos-08-fruit.mp4', 'mp4', 9757670, 'CGJdRReNUkUF9zjt'),
(23, 'yOLvqwv2T3uPzp6u.mp4', 'find-in-laos-09-family.mp4', 'mp4', 11586248, 'bDsHGUidG2YY8e5i'),
(24, 'LYHqklGzTecvju4H.mp4', 'find-in-laos-10-question-words.mp4', 'mp4', 3216594, 'ncs2VUo4GmaIksut'),
(26, 'HX6RvXKHvlvU69ak.mp4', 'find-in-laos-11-locations.mp4', 'mp4', 2478354, 'CyIOAfQ8hbxjmbiE'),
(27, 'qtcgPZcYf1II1NcG.mp4', 'find-in-laos-12-actions.mp4', 'mp4', 12583496, 'USJ5EK6MjJS3wd6X'),
(28, 'KfahwcXGiXvbishp.mp4', 'find-in-laos-13-emotions-and-adjectives.mp4', 'mp4', 19665628, 'LkY1Nhek3IDIPLMu'),
(29, 'FRLKwjXSZLo70uJg.mp4', 'find-in-laos-14-colors.mp4', 'mp4', 6697610, 'iTM9uJMAqsIb2sR7'),
(30, 'bD4h2JS1MNZPYWby.mp4', 'find-in-laos-15-time.mp4', 'mp4', 9011078, '7N74qmnMKQTW8axy'),
(31, 'AwujkCiEcpoRcXth.mp4', 'find-in-laos-16-vocabulary-about-school.mp4', 'mp4', 5171207, 'hjjnLMQAJqDA2bL9'),
(32, '7IzvX6SXH3A4OxUQ.mp4', 'find-in-laos-18-vocabulary-about-the-temple.mp4', 'mp4', 5422621, '9zVt4tC6bt1U7S83'),
(33, 'xknHZK3HwOJaBABe.mp4', 'find-in-laos-19-various-festivals.mp4', 'mp4', 6315848, 'l9cGxiUYKXu5dfuP'),
(34, '3YXHn3UA80qBxG0w.mp4', 'find-in-laos-20-helping-children-who-are-deaf-credits.mp4', 'mp4', 5307404, 'rAtHw0C1v5fI8kqi'),
(35, '5I3chwqq16o0sDlC.pdf', 'gape-baeng-ban-kwam-hu-su-sum-son.pdf', 'pdf', 3196203, '6K4OC14GN5DQ8CSw'),
(36, 'PfhVea1JKu8RxQnT.pdf', 'gape-nitan-feun-ban-pao-ta-oy.pdf', 'pdf', 4255152, '6RI2rVPxIJrGRiLR'),
(37, 'aB97NV6WB4nSPJlN.pdf', 'gape-paendin-keu-sivi.pdf', 'pdf', 3413483, 'dS1OZCc8vI3VHoHo'),
(38, 'T14PaMTawjI6w5r0.pdf', 'hesperian-yu-gai-paet.pdf', 'pdf', 7732145, 'wjiWWumCtVyT8Nq7'),
(39, 'Qj1pXLjc40QnUjKa.mp4', 'ka-xiong-intellectual-property-rights.mp4', 'mp4', 2856896, 'DoKKErz8VPOO7G5E'),
(40, 'eVcUpACWcrpHkuSc.mp4', 'ka-xiong-nutrition.mp4', 'mp4', 5440491, '0C6pOzvWolGamzks'),
(41, 'wM28J44wl91OpGik.mp4', 'ka-xiong-think-before-you-act.mp4', 'mp4', 35506085, 'zGhffpoHSy4JLgGH'),
(42, '5GdGfPLSR7Wbt3e2.mp4', 'ksouthis-lao-diabetes-education-part-2.mp4', 'mp4', 32422088, 'a5KstiVAEqCcWWMx'),
(43, 'jXA4kZEEVuW3Zz8a.mp4', 'ksouthis-lao-diabetes-education-part-1.mp4', 'mp4', 34783356, 'XWkuOL2KX6wzaUbi'),
(44, '1GUWcODtK6f4lUtd.mp4', 'ksouthis-lao-diabetes-education-part-3.mp4', 'mp4', 36223445, 'bLOAZ7kkwkSAMOr8'),
(45, '8wxthim9avD5eMDF.mp4', 'ksouthis-lao-diabetes-education-part-4.mp4', 'mp4', 32915979, 'a1vnUEqzAccB1CkD'),
(46, 'ARvwqEYLdlujBimA.pdf', 'language-project-bounmy-has-a-buffalo.pdf', 'pdf', 3348424, 'uuKI69X8gWClWZmQ'),
(47, 'U3kJozNmQm9ErJIT.mp4', 'language-project-bounmy-has-a-buffalo.mp4', 'mp4', 9298250, 'n3iZJUjUcUSBu4o5'),
(48, 'nNBDkaj0ix27TBSa.pdf', 'language-project-english-for-ecoguides.pdf', 'pdf', 49491161, 'zedoPDwaoqH80lM8'),
(49, 'EuCbxDHLAyK7pa8S.mp4', 'language-project-gan-hae-toa-korng-si.mp4', 'mp4', 21483365, 'mlym5EP6zJutflR6'),
(50, 'wqxWPmVUDpwym7F7.mp4', 'language-project-gan-loi-toa-korng-kwam-horn.mp4', 'mp4', 10349166, 'w08XoulUWEJ1bUio'),
(51, 'GDxQuGNfu5U30Hn8.mp4', 'language-project-lek-mae-gon-dorn-1.mp4', 'mp4', 10956972, '2tB3gfqa3DOwWh6U'),
(52, '65txXGqS7bDzz4DH.mp4', 'language-project-lek-mae-gon-dorn-2.mp4', 'mp4', 13184229, 'HK5gNPK7u8lBtQJW'),
(53, 'dkPOnDHUhgXK03Ra.mp4', 'national-art-center-forn-jampa-meuang-lao.mp4', 'mp4', 26562051, 'T7WLbGVNj6vWap61'),
(54, 'nXe10UWYzQd9FR02.mp4', 'nop-studio-learn-lao-alphabet-in-two-minutes.mp4', 'mp4', 5361842, 'VSNWbO51d0w8rha9'),
(55, 'zRALsXePu0IoNZwt.pdf', 'project-anoulak-beum-lin-gaem-giao-gap-ling-kang-ta-lung.pdf', 'pdf', 10581092, '8OsmexR9LHTkpRyj'),
(56, 'D6JsNeSquh7QtV7l.pdf', 'project-anoulak-soi-koun-korng-doukki.pdf', 'pdf', 36285395, 'ZlLIASg969n4zbwv'),
(57, 'EgfaHXBNMsIh7sOO.mp4', 'taf-4th-of-july.mp4', 'mp4', 8198979, '6irJkEnhjqntbrNB'),
(58, '7oL8BJA7Ct4kN8SV.mp4', 'taf-articles.mp4', 'mp4', 11742857, 'QpNxmR3qrPv44B7B'),
(59, 'vgPyOsz1FfQNXVaG.mp4', 'taf-ask-better-questions.mp4', 'mp4', 7557877, 'YNpS7EKr4pzTzZ6A'),
(60, 'Egfzfde3FbgN4k2h.mp4', 'taf-could-should-would.mp4', 'mp4', 12045940, 'xGqZaPXGg3MAL37b'),
(61, 'BCp7zIwMRXNpZ2UW.mp4', 'taf-hit-on-them.mp4', 'mp4', 10753579, 'sm5BwS9CZ7mDljkV'),
(62, 'CrKbQylV7LOWCUTV.mp4', 'taf-hows-it-going.mp4', 'mp4', 12239589, 'l5uSsX0q0Dn92awC'),
(63, 'hh6QUjztsicTpOkN.mp4', 'taf-lao-vs-brazil.mp4', 'mp4', 12891958, 'wkv1OYRm0bvHZuDh'),
(64, 'Yf23FuNUUjljlqlb.mp4', 'taf-learning-f-and-v-sound.mp4', 'mp4', 12296903, 'PZ1j3SOy2viCEVY0');

-- --------------------------------------------------------

--
-- Table structure for table `tb_mainmenu`
--

CREATE TABLE `tb_mainmenu` (
  `id` int(25) NOT NULL,
  `mainmenu_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mainmenu_type` int(6) NOT NULL,
  `mainmenu_detail` longtext COLLATE utf8_unicode_ci,
  `mainmenu_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `m_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mainmenu_sorting` int(6) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `create_by` int(13) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_mainmenu`
--

INSERT INTO `tb_mainmenu` (`id`, `mainmenu_name`, `mainmenu_type`, `mainmenu_detail`, `mainmenu_url`, `m_url`, `mainmenu_sorting`, `created_at`, `updated_at`, `create_by`) VALUES
(8, 'ຕິດຕໍ່ພວກເຮົາ (Contact us)', 2, '', 'http://', 'ຕິດຕໍ່ພວກເຮົາ-(Contact-us)', 0, '2015-11-21 22:18:34', '2015-11-21 22:18:34', 1),
(9, 'ກ່ຽວກັບເວັບໃຊ້ (About Lao44)', 1, '<p>ແັກຫດອຫດຶກຫດືກືກັຫດຶ&nbsp;ັກຫຶຫກຶຫກອກ</p>\r\n', 'http://', 'ກ່ຽວກັບເວັບໃຊ້-(About-Lao44)', 0, '2015-12-21 10:11:58', '2015-12-23 14:07:08', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_submenu`
--

CREATE TABLE `tb_submenu` (
  `id` int(25) NOT NULL,
  `submenu_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `submenu_categories` int(6) NOT NULL,
  `submenu_type` int(6) NOT NULL,
  `submenu_detail` longtext COLLATE utf8_unicode_ci,
  `submenu_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `s_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `create_by` int(13) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_tag`
--

CREATE TABLE `tb_tag` (
  `id` int(25) NOT NULL,
  `tag_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tag_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tag_count` int(15) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `create_by` int(13) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_tag`
--

INSERT INTO `tb_tag` (`id`, `tag_name`, `tag_url`, `tag_count`, `created_at`, `updated_at`, `create_by`) VALUES
(6, 'facebook', 'facebook', 0, '2015-12-21 17:01:13', '2015-12-21 17:01:13', 1),
(7, 'IT', 'IT', 0, '2015-12-21 17:01:22', '2015-12-21 17:01:22', 1),
(8, 'ການຜະລິດກະສິກຳແບບມີສັນຍາ contract farming', 'ການຜະລິດກະສິກຳແບບມີສັນຍາ-contract-farming', 1, '2015-12-21 17:01:50', '2015-12-22 12:31:46', 1),
(9, 'ການຜະລິດກະສິກຳເປັນສີນຄ້າ commercial agriculture', 'ການຜະລິດກະສິກຳເປັນສີນຄ້າ-commercial-agriculture', 1, '2015-12-21 17:02:22', '2015-12-22 12:00:03', 1),
(10, 'ເຕັກນິກການຜະລິດກະສິກຳ farming techniques', 'ເຕັກນິກການຜະລິດກະສິກຳ-farming-techniques', 0, '2015-12-21 17:02:59', '2015-12-21 17:02:59', 1),
(11, 'ເຄື່ອງປ່າຂອງດົງ NTFPs', 'ເຄື່ອງປ່າຂອງດົງ-NTFPs', 0, '2015-12-21 17:03:51', '2015-12-21 17:03:51', 1),
(12, 'ສຸຂະພາບ Health', 'ສຸຂະພາບ-Health', 0, '2015-12-21 17:04:42', '2015-12-21 17:04:42', 1),
(13, 'ວັນນະຄະດີ', 'ວັນນະຄະດີ', 0, '2015-12-21 17:05:19', '2015-12-21 17:05:19', 1),
(14, 'ບົດລາຍງານການສຶກສາ study reports', 'ບົດລາຍງານການສຶກສາ-study-reports', 0, '2015-12-21 17:05:52', '2015-12-21 17:05:52', 1),
(15, 'ຄູ່ມື manual ', 'ຄູ່ມື-manual-', 2, '2015-12-21 17:06:15', '2016-01-12 14:49:17', 1),
(16, 'ຫລັກສູດ curriculum', 'ຫລັກສູດ-curriculum', 2, '2015-12-21 17:06:48', '2016-01-13 16:33:47', 1),
(17, 'ການຄຸ້ມຄອງຊັບພະຍາກອນທຳມະຊາດ natural resource management ', 'ການຄຸ້ມຄອງຊັບພະຍາກອນທຳມະຊາດ-natural-resource-management-', 0, '2015-12-21 17:35:02', '2015-12-21 17:35:02', 1),
(18, 'ການສົ່ງເສີມກະສິກຳ agriculture extension', 'ການສົ່ງເສີມກະສິກຳ-agriculture-extension', 0, '2015-12-21 17:35:32', '2015-12-21 17:35:32', 1),
(19, 'ອົງການຈັດຕັ້ງຊາວກະສິກອນ farmer organization', 'ອົງການຈັດຕັ້ງຊາວກະສິກອນ-farmer-organization', 5, '2015-12-21 17:35:58', '2016-01-13 16:42:20', 1),
(21, 'ບົດນຳສະເຫນີ presentation', 'ບົດນຳສະເຫນີ-presentation', 0, '2015-12-21 17:36:54', '2015-12-21 17:36:54', 1),
(22, 'ການຄຸ້ມຄອງປ່າໄມ້ forestry management ', 'ການຄຸ້ມຄອງປ່າໄມ້-forestry-management-', 0, '2015-12-21 17:37:34', '2015-12-21 17:37:34', 1),
(23, 'ເຕັກນິກການລ້ຽງສັດ livestock raising techniques', 'ເຕັກນິກການລ້ຽງສັດ-livestock-raising-techniques', 0, '2015-12-21 17:38:10', '2015-12-21 17:38:10', 1),
(24, 'ການປະມົງ fishery', 'ການປະມົງ-fishery', 0, '2015-12-21 17:38:29', '2015-12-21 17:38:29', 1),
(26, 'ແຜນທີ່ maps', 'ແຜນທີ່-maps', 0, '2015-12-21 17:39:28', '2015-12-21 17:39:28', 1),
(27, 'social media', 'social-media', 0, '2015-12-21 17:40:04', '2015-12-21 17:40:04', 1),
(28, 'ໄວຫນຸ່ມ youth', 'ໄວຫນຸ່ມ-youth', 0, '2015-12-21 17:40:20', '2015-12-21 17:40:20', 1),
(32, 'ວິດີໂອ video', 'ວິດີໂອ-video', 2, '2015-12-21 17:42:42', '2016-01-12 14:48:06', 1),
(33, 'ສີ່ງບັນເທີງ entertainment ', 'ສີ່ງບັນເທີງ-entertainment-', 0, '2015-12-21 17:43:18', '2015-12-21 17:43:18', 1),
(34, 'ວັດທະນາທຳລາວ Lao culture', 'ວັດທະນາທຳລາວ-Lao-culture', 0, '2015-12-21 17:55:36', '2015-12-21 17:55:36', 1),
(35, 'ພາສາອັງກິດ English language', 'ພາສາອັງກິດ-English-language', 0, '2015-12-22 12:21:00', '2015-12-22 12:21:00', 1),
(36, 'ກົດຫມາຍ Laws', 'ກົດຫມາຍ-Laws', 0, '2015-12-22 13:50:02', '2015-12-22 13:50:48', 1),
(37, 'ສິດ Rights', 'ສິດ-Rights', 0, '2015-12-22 13:50:38', '2015-12-22 13:50:38', 1),
(38, 'ນະໂຍບາຍ Policies', 'ນະໂຍບາຍ-Policies', 0, '2015-12-22 13:51:15', '2015-12-22 13:51:15', 1),
(39, 'ຍຸດທະສາດ Strategies', 'ຍຸດທະສາດ-Strategies', 0, '2015-12-22 13:51:45', '2015-12-22 13:51:45', 1),
(40, 'ນິທານ Folk story', 'ນິທານ-Folk-story', 0, '2015-12-22 13:59:04', '2015-12-22 13:59:04', 1),
(42, 'ໂພສະນາການ Nutrition', 'ໂພສະນາການ-Nutrition', 0, '2015-12-22 14:17:02', '2015-12-22 14:17:02', 1),
(43, 'ລົນນະລົງ campaign and awareness raising', 'ລົນນະລົງ-campaign-and-awareness-raising', 0, '2015-12-22 14:22:14', '2015-12-22 14:22:14', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_tagcontent`
--

CREATE TABLE `tb_tagcontent` (
  `id` int(11) NOT NULL,
  `tag_id` int(6) NOT NULL,
  `content_id` int(15) NOT NULL,
  `tag_count` int(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_tagcontent`
--

INSERT INTO `tb_tagcontent` (`id`, `tag_id`, `content_id`, `tag_count`) VALUES
(7, 13, 12, 0),
(8, 5, 12, 0),
(9, 30, 11, 0),
(16, 27, 11, 0),
(17, 7, 11, 0),
(18, 6, 11, 0),
(19, 21, 10, 0),
(20, 14, 10, 0),
(21, 10, 10, 0),
(22, 9, 10, 0),
(23, 8, 10, 0),
(24, 5, 10, 0),
(25, 22, 9, 0),
(26, 21, 9, 0),
(27, 11, 9, 0),
(28, 5, 9, 0),
(29, 33, 8, 0),
(30, 32, 8, 0),
(31, 28, 8, 0),
(32, 27, 8, 0),
(33, 7, 8, 0),
(34, 6, 8, 0),
(35, 33, 7, 0),
(36, 32, 7, 0),
(37, 28, 7, 0),
(38, 7, 7, 0),
(39, 6, 7, 0),
(40, 32, 13, 0),
(41, 26, 13, 0),
(42, 5, 13, 0),
(43, 34, 14, 0),
(44, 33, 14, 0),
(45, 32, 14, 0),
(46, 10, 14, 0),
(47, 32, 15, 0),
(49, 32, 16, 0),
(51, 32, 17, 0),
(53, 32, 18, 0),
(55, 32, 19, 0),
(57, 32, 20, 0),
(59, 32, 21, 0),
(62, 16, 21, 0),
(64, 16, 20, 0),
(66, 16, 19, 0),
(68, 16, 18, 0),
(70, 16, 17, 0),
(72, 16, 16, 0),
(76, 16, 15, 0),
(77, 35, 22, 0),
(78, 32, 22, 0),
(79, 16, 22, 0),
(80, 35, 23, 0),
(81, 32, 23, 0),
(82, 16, 23, 0),
(83, 35, 24, 0),
(84, 32, 24, 0),
(85, 16, 24, 0),
(86, 35, 26, 0),
(87, 32, 26, 0),
(88, 16, 26, 0),
(89, 35, 27, 0),
(94, 33, 27, 0),
(95, 16, 27, 0),
(96, 35, 28, 0),
(97, 32, 28, 0),
(98, 16, 28, 0),
(99, 35, 29, 0),
(100, 32, 29, 0),
(101, 16, 29, 0),
(102, 35, 30, 0),
(103, 32, 30, 0),
(104, 16, 30, 0),
(105, 35, 31, 0),
(106, 32, 31, 0),
(107, 16, 31, 0),
(108, 35, 33, 0),
(109, 32, 33, 0),
(110, 16, 33, 0),
(111, 35, 34, 0),
(112, 32, 34, 0),
(113, 16, 34, 0),
(115, 5, 35, 0),
(118, 34, 36, 0),
(119, 33, 36, 0),
(120, 13, 36, 0),
(121, 37, 37, 0),
(122, 34, 37, 0),
(123, 33, 37, 0),
(124, 28, 37, 0),
(125, 13, 37, 0),
(126, 15, 38, 0),
(127, 37, 39, 0),
(128, 36, 39, 0),
(129, 32, 39, 0),
(130, 32, 40, 0),
(132, 32, 41, 0),
(134, 43, 43, 0),
(135, 32, 43, 0),
(136, 12, 43, 0),
(137, 32, 44, 0),
(138, 43, 45, 0),
(139, 32, 45, 0),
(140, 35, 46, 0),
(141, 15, 46, 0),
(142, 35, 47, 0),
(143, 32, 47, 0),
(144, 35, 48, 0),
(145, 32, 48, 0),
(146, 35, 49, 0),
(147, 15, 49, 0),
(148, 32, 50, 0),
(149, 32, 51, 0),
(150, 32, 52, 0),
(151, 33, 53, 0),
(152, 32, 53, 0),
(153, 32, 54, 0),
(154, 33, 55, 0),
(155, 28, 55, 0),
(156, 15, 55, 0),
(157, 43, 56, 0),
(158, 35, 56, 0),
(159, 35, 57, 0),
(160, 32, 57, 0),
(161, 16, 57, 0),
(162, 35, 58, 0),
(163, 32, 58, 0),
(164, 16, 58, 0),
(165, 35, 59, 0),
(166, 32, 59, 0),
(167, 16, 59, 0),
(168, 35, 60, 0),
(169, 32, 60, 0),
(170, 16, 60, 0),
(171, 35, 61, 0),
(172, 32, 61, 0),
(173, 16, 61, 0),
(174, 35, 62, 0),
(175, 32, 62, 0),
(176, 16, 62, 0),
(177, 35, 63, 0),
(178, 32, 63, 0),
(179, 16, 63, 0),
(180, 35, 64, 0),
(181, 32, 64, 0),
(182, 16, 64, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(25) NOT NULL,
  `username` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_status` int(3) NOT NULL,
  `user_type` int(11) NOT NULL,
  `user_code` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `user_status`, `user_type`, `user_code`, `created_at`, `updated_at`, `remember_token`) VALUES
(1, 'admin', '$2y$10$tO8QkExsueZVRDDWKuDHmeIt.dE6s6jI0lgvFADE4ANU8ZNDpYKlu', 1, 1, '', '2015-10-20 00:00:00', '2016-01-13 16:34:12', 'w9LsNlnOMBDjw7G7ScCRoat1gae0W2SjgiKIfTlfci6KNtxw4qiGedmTZtzd');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_categories`
--
ALTER TABLE `tb_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_content`
--
ALTER TABLE `tb_content`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_enviroment`
--
ALTER TABLE `tb_enviroment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_files`
--
ALTER TABLE `tb_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_mainmenu`
--
ALTER TABLE `tb_mainmenu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_submenu`
--
ALTER TABLE `tb_submenu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_tag`
--
ALTER TABLE `tb_tag`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_tagcontent`
--
ALTER TABLE `tb_tagcontent`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_categories`
--
ALTER TABLE `tb_categories`
  MODIFY `id` int(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `tb_content`
--
ALTER TABLE `tb_content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;
--
-- AUTO_INCREMENT for table `tb_enviroment`
--
ALTER TABLE `tb_enviroment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_files`
--
ALTER TABLE `tb_files`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;
--
-- AUTO_INCREMENT for table `tb_mainmenu`
--
ALTER TABLE `tb_mainmenu`
  MODIFY `id` int(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tb_submenu`
--
ALTER TABLE `tb_submenu`
  MODIFY `id` int(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_tag`
--
ALTER TABLE `tb_tag`
  MODIFY `id` int(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT for table `tb_tagcontent`
--
ALTER TABLE `tb_tagcontent`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=183;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
